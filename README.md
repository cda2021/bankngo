# BankNGo

This project is  the result of a school project named "BankNGo".

In this project, we have to model an online bank service with many features :

Users can :

- Register

- Ask to open a bank account.

- Ask to close a bank account.

- Have some recipients

- Make cash transfer to their recipients

- See their account balance
  
  

Bankers can :

- See what all happends from the users (kind of administration)

- Accept or deny opening/closing bank account requests

- Accept or deny adding of beneficients

## The project

### Architecture

It's based on the Symfony 4 architecture. We choosed the design pattern **Factory** to include our services into our actions (Action Domain Responder).
https://refactoring.guru/design-patterns/abstract-factory/php/example#lang-features

#### Launch the project

⚠ Docker is required to lanch the project, see how to install Docker (and docker-compose)  [see how to get Docker](https://docs.docker.com/get-docker/).

```shell
docker-compose up
```

It will install containers / require all dependencies / make the database migrations and load fixtures.

 Then until PHP says that he's ready, you can launch your browser at [](localhost:8000).

### VCS

We works as One issue => One branch/merge request.
