/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import 'bootstrap'
import 'bootstrap/js/dist/toast'
import 'bootstrap/js/dist/collapse'

const Turbolinks = require("turbolinks");
Turbolinks.start();

const $ = require('jquery');
import "slicknav/jquery.slicknav"
const $mainWindow = $(window);

$mainWindow.on("scroll", function () {
    let scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $(".scrolling-navbar").addClass("top-nav-collapse");
    } else {
        $(".scrolling-navbar").removeClass("top-nav-collapse");
    }
});

let logo_path = $(".mobile-menu").data("logo");
$("#main-navbar").slicknav({
    appendTo: ".mobile-menu",
    removeClasses: false,
    label: "",
    closedSymbol: '<i class="lni-chevron-right"><i/>',
    openedSymbol: '<i class="lni-chevron-down"><i/>',
    brand:
        '<a href="index.html"><img src="' +
        logo_path +
        '" class="img-responsive" alt="logo"></a>',
});

$(function($) {
    discover();
    toastAndCollapse();
});

function toast(time, data) {
    let template = [
        '<div class="toast" id="'+ data.id +'">',
            '<div class="toast-header">',
                '<div class="bg-success rounded mr-2" style="height: 20px; width: 20px;"></div>',
                '<strong class="mr-auto">New notification</strong>',
                '<small>Now</small>',
                '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">',
                    '<span aria-hidden="true">&times;</span>',
                '</button>',
            '</div>',
            '<div class="toast-body">' + data.message + '</div>',
        '</div>'
    ].join("\n");

    return template;
}

function discover() {
    fetch("/discover", {method: "GET"}).then((response) => {
        if (response.ok) {
            response.json().then((data) => {
                let hubUrl = response.headers.get('Link').match(/<([^>]+)>;\s+rel=(?:mercure|"[^"]*mercure[^"]*")/)[1];
                const hub = new URL(hubUrl);
                hub.searchParams.append('topic', data.topic)

                const eventSource = new EventSource(hub, {
                    withCredentials: true
                });

                eventSource.onmessage = event => {
                    let data = JSON.parse(event.data);
                    let time = new Date(data.timestamp * 1000);
                    data.id = Math.random().toString(36).substring(7);
                    let template = toast(time, data);

                    $("#toast-container").append(template);
                    $('.toast').toast({
                        animation: true,
                        autohide: false,
                        delay: 15000
                    });

                    let selector = "#" + data.id;
                    $(selector).toast("show");
                };
            })
        }
    })
}

function toastAndCollapse() {
    const $toast = $('.toast');
    const $collapse = $('.collapse');

    $toast.toast({
        animation: true,
        autohide: false,
        delay: 15000
    });

    $collapse.collapse({
        toggle: false
    });

    $collapse.collapse();
    $toast.toast("show");
}
