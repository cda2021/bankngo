const $ = require('jquery');
const dt = require('datatables.net-bs4');

function dataTable() {
    let dataTable = $("#datatable").DataTable({
        stateSave: true,
        fixedHeader: true
    });
}

$(function () {
    dataTable();
    document.addEventListener("turbolinks:load", function() {
        dataTable()
    });
});


