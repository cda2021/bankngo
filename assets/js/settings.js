const $ = require('jquery');

const $g2FaBloc = $("#g-auth-presentation");
const $input = $("#user_settings_twoFactorAuthentification");

$input.change(function () {
    let result = $(this).prop('checked');

    if (result === true) {
        $g2FaBloc.show();
    } else {
        $g2FaBloc.hide();
    }
});
