const $ = require("jquery");
let $password = $("#registration_form_plainPassword");

$(document).ready(function () {
    $("#formRegister").on("submit", function()
    {
        let result = true;

        const regexPassword = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})")

        if (!regexPassword.test($password.val()))
        {
            result = false;
            $password.css({
                "borderColor": "red"
            })
        }

        return result;
    });

    $(".passwordicon").on("click", function (e) {
        let classList = $(this).get(0).classList.value;
        if (classList.includes("fa-eye-slash")) {
            $(this).addClass("fa-eye");
            $(this).removeClass("fa-eye-slash");
            $password.get(0).type = "text";
        } else {
            $(this).addClass("fa-eye-slash");
            $(this).removeClass("fa-eye");
            $password.get(0).type = "password";
        }
    });
});
