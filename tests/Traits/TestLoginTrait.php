<?php


namespace App\Tests\Traits;


use Symfony\Component\HttpFoundation\Request;

trait TestLoginTrait
{
    public function loginAsVerifiedUser()
    {
        $crawler = $this->client->request(Request::METHOD_GET, '/login');

        $form = $crawler->selectButton('Login')->form([
            "uniqueLogin" => '5fd7734f3f048',
            "password" => 'riri'
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();
        $this->client->followRedirect();
    }

    public function loginAsNotVerifiedUser()
    {
        $crawler = $this->client->request(Request::METHOD_GET, '/login');

        $form = $crawler->selectButton('Login')->form([
            "uniqueLogin" => '5fd79679c7288',
            "password" => 'notverified'
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();
        $this->client->followRedirect();
    }

    public function loginAsBanker()
    {
        $crawler = $this->client->request(Request::METHOD_GET, '/login');

        $form = $crawler->selectButton('Login')->form([
            "uniqueLogin" => '5fd7967abb0e1',
            "password" => 'banker'
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();
        $this->client->followRedirect();
    }

    public function loginAsAdmin()
    {
        $crawler = $this->client->request(Request::METHOD_GET, '/login');

        $form = $crawler->selectButton('Login')->form([
            "uniqueLogin" => '5fd7734e768b1',
            "password" => 'oucema'
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();
        $this->client->followRedirect();
    }

    /**
     * Debug tool.
     */
    public function dumpResponse()
    {
        var_dump($this->client->getResponse()->getContent());
        die();
    }
}
