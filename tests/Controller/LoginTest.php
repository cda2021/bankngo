<?php


namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class LoginTest extends WebTestCase
{
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testLogin()
    {
        $crawler = $this->client->request(Request::METHOD_GET, '/login');

        $form = $crawler->selectButton('Login')->form([
            "uniqueLogin" => '5fd7734f3f048',
            "password" => 'riri'
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();
    }
}
