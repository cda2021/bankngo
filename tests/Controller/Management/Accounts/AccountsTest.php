<?php


namespace App\Tests\Controller\Management\Accounts;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class AccountsTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testAccounts()
    {
        $this->loginAsBanker();
        $crawler = $this->client->request(Request::METHOD_GET, '/management/accounts/');

        $this->assertResponseIsSuccessful();
        $this->assertEquals("Manage accounts", $crawler->filter("body > div.section.mt-5 > div > div > div.col-lg-8.col-md-12.col-xs-12 > div > h3")->text());
    }
}