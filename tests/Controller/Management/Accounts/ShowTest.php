<?php


namespace App\Tests\Controller\Management\Accounts;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ShowTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testShowBankerUser()
    {
        $this->loginAsBanker();
        $crawler = $this->client->request(Request::METHOD_GET, '/management/accounts/0dc230a9-02db-4f84-b442-f8d3c4adfcee');

        $this->assertResponseIsSuccessful();
        $this->assertEquals("Account BE78311444845059", $crawler->filter("body > div.section.mt-5 > div > div > div.col-lg-8.col-md-12.col-xs-12 > div > h3")->text());
    }

    public function testShowCustomerUser()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, '/management/accounts/0dc230a9-02db-4f84-b442-f8d3c4adfcee');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

}