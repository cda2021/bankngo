<?php


namespace App\Tests\Controller\Management;

use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ManagementTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testAdminPagesWithBankerUser()
    {
        $this->loginAsBanker();
        $crawler = $this->client->request(Request::METHOD_GET, '/management');

        $this->assertResponseIsSuccessful();
        $this->assertEquals("Requests Account received", $crawler->filter("#card-box > div > div > div:nth-child(1) > div > div > h3 > a")->text());
    }

    public function testAdminPagesWithCustomerUser()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, '/management');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }
}
