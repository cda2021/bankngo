<?php


namespace App\Tests\Controller\Management\Requests\Recipient;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RecipientRequestTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testRecipientRequestBankerUser()
    {
        $this->loginAsBanker();
        $crawler = $this->client->request(Request::METHOD_GET, '/management/recipient/requests');

        $this->assertResponseIsSuccessful();
        $this->assertEquals("Recipient requests", $crawler->filter("body > div.section.mt-5 > div > div > div.col-lg-8.col-md-6.col-xs-12 > div > h3")->text());
    }

    public function testRecipientRequestCustomerUser()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, '/management/recipient/requests');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }
}