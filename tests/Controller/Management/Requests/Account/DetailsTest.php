<?php


namespace App\Tests\Controller\Management\Requests\Account;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DetailsTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testDetailsBankerUser()
    {
        $this->loginAsBanker();
        $crawler = $this->client->request(Request::METHOD_GET, '/management/account/requests/details/0c17e18f-a8b7-4e33-95d8-cd23947824b6');

        $this->assertResponseIsSuccessful();
        $this->assertEquals("Request at: Verified Ri", $crawler->filter("body > div.section.mt-5 > div > div > div.col-lg-8.col-md-12.col-xs-12 > div > h3")->text());
    }

    public function testDetailsCustomerUser()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, '/management/account/requests/details/0c17e18f-a8b7-4e33-95d8-cd23947824b6');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }
}