<?php


namespace App\Tests\Controller\Management\Requests\Account;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountRequestTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testAccountRequestBankerUser()
    {
        $this->loginAsBanker();
        $crawler = $this->client->request(Request::METHOD_GET, '/management/account/requests');

        $this->assertResponseIsSuccessful();
        $this->assertEquals("Account requests", $crawler->filter("body > div.section.mt-5 > div > div > div.col-lg-8.col-md-6.col-xs-12 > div > h3")->text());
    }

    public function testAccountRequestCustomerUser()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, '/management/account/requests');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }
}