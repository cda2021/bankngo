<?php


namespace App\Tests\Controller\Portal\Account;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class AccountTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;
    public const fixturesDirectory = __DIR__ . "/../../../Fixtures/";
    public const titleSelector = "#card-box > div > div > div.col-md-12.row > div.col-md-7 > h3";

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @group portal_account
     */
    public function testUserHavingAccount()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, "/portal");
        $this->assertResponseIsSuccessful();

        $link = $crawler->filter("#card-box > div > div > div.col-md-6.col-xs-12 > div > div > h3 > a");
        $this->assertEquals("Compte courant", $link->text());

        $crawler = $this->client->request(Request::METHOD_GET, $link->attr("href"));

        $this->assertEquals("Compte courant",
            $crawler->filter("body > div.section > div > div > div.col-lg-8.col-md-12.col-xs-12 > div > h3")->text());
    }

    /**
     * @group portal_account
     */
    public function testUserAskingForCloseAccount()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, "/portal");
        $this->assertResponseIsSuccessful();

        $link = $crawler->filter("#card-box > div > div > div.col-md-6.col-xs-12 > div > div > h3 > a");
        $this->assertEquals("Compte courant", $link->text());

        $crawler = $this->client->request(Request::METHOD_GET, $link->attr("href"));

        $this->assertEquals("IBAN: BE78311444845059",
            $crawler->filter("body > div.section > div > div > div.col-lg-8.col-md-12.col-xs-12 > div > ul > li:nth-child(2)")->text());

        $closeButton = $crawler->filter("div.col-lg-8.col-md-12.col-xs-12 > div > div > a.btn.btn-danger.m-3");

        $this->assertEquals("Close account", $closeButton->text());
        $crawler = $this->client->request(Request::METHOD_GET, $closeButton->attr("href"));
        $this->assertResponseIsSuccessful();
        $this->assertEquals("Close account request",
            $crawler->filter("div.col-lg-8.col-md-12.col-xs-12 > div > h3")->text());

        $declarationFile = new UploadedFile(
            self::fixturesDirectory . "declaration.pdf",
            "declaration.pdf",
            "application/pdf",
            null
        );

        $form = $crawler->selectButton("Send")->form([
            "account_request[file]" => $declarationFile
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();
        $crawler = $this->client->followRedirect();

        $this->assertEquals("Your closure account request have been received by an advisor. You will be notified by mail when it will be processed.",
            $crawler->filter("body > section > div > div > div > div.toast-body")->text());
    }

    /**
     * @group portal_account
     */
    public function testUserAskingForNewAccount()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, "/portal/request/new");
        $this->assertResponseIsSuccessful();
        $this->assertEquals("New account request",
            $crawler->filter("div.col-lg-8.col-md-12.col-xs-12 > div > h3")->text());

        $idCard = new UploadedFile(
            self::fixturesDirectory . "id-card.jpg",
            "id-card.jpg",
            "image/jpeg",
            null
        );

        $form = $crawler->selectButton("Send")->form([
            "account_request[overdraftAuthorized]" => true,
            "account_request[file]" => $idCard
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();

        $crawler = $this->client->followRedirect();

        $this->assertEquals("Your account request have been received by an advisor.",
            $crawler->filter("body > section > div > div > div > div.toast-body")->text());
    }
}
