<?php


namespace App\Tests\Controller\Portal\Recipient;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class RecipientTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testRecipientPage()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, "/portal/recipients/");
        $this->assertResponseIsSuccessful();

        $this->assertEquals("My recipients",
            $crawler->filter("#card-box > div > div > div.col-md-12.row > div.col-md-7 > h3")->text());
        $this->assertEquals("You do not have any recipient.", $crawler->filter("div.alert.alert-dark")->text());
    }

    public function testAddRecipient()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, "/portal/recipients/add");
        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton("Send")->form([
            "recipient_request[name]" => "John",
            "recipient_request[surname]" => "Doe",
            "recipient_request[iban]" => "GE86NY2298621592807611",
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();
        $crawler = $this->client->followRedirect();

        $this->assertEquals("Your recipient request have been received by an advisor.",
            $crawler->filter("body > section > div > div > div > div.toast-body")->text());
    }
}
