<?php


namespace App\Tests\Controller\Portal;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testPortalNotLogged()
    {
        $this->client->request(Request::METHOD_GET, "/portal");

        $this->assertResponseRedirects();
        $crawler = $this->client->followRedirect();

        $this->assertEquals("Log in", $crawler->filter("#content > div > div > div > div > h3")->text());
    }

    public function testPortalLogged()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, "/portal");
        $this->assertResponseIsSuccessful();
        $this->assertEquals("Ri Verified", $crawler->filter("body > div.page-header > div > div > div > div > h3")->text());
    }

    public function testPortalLoggedNotVerified()
    {
        $this->loginAsNotVerifiedUser();
        $this->client->request(Request::METHOD_GET, "/portal");
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }
}
