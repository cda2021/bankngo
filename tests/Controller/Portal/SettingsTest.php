<?php


namespace App\Tests\Controller\Portal;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SettingsTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testSettings()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, "/portal/settings");

        $this->assertResponseIsSuccessful();
        $this->assertEquals("Settings", $crawler->filter("div.col-lg-8.col-md-12.col-xs-12 > div > h3")->text());
        $this->assertEquals("verified@user.com", $crawler->filter("#user_settings_email")->attr("value"));

        $form = $crawler->selectButton("Save")->form([
            "user_settings[phoneNumber]" => "0600000000",
            "user_settings[email]" => "very@verified.com"
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();
        $crawler = $this->client->followRedirect();

        $this->assertEquals("Your settings have been saved.", $crawler->filter("body > section > div > div > div > div.toast-body")->text());
    }
}
