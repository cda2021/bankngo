<?php


namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class RegisterTest extends WebTestCase
{
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testRegister()
    {
        $crawler = $this->client->request(Request::METHOD_GET, '/register');

        $form = $crawler->selectButton('Register')->form([
            "registration_form[email]" => 'test.test@cfa-afti.fr',
            "registration_form[plainPassword]" => 'Testnum1!',
            'registration_form[agreeTerms]' => true,
            "registration_form[name]" => "TestNom",
            "registration_form[surname]" => "TestPrenom",
            "registration_form[birthDate]" => "1999-11-10T00:00:00",
            "registration_form[phoneNumber]" => "0639342934",
            "registration_form[gender]" => "M",
        ]);

        $this->client->submit($form);
        $this->assertResponseRedirects();
    }

    public function testRegisterAsTooYoung()
    {
        $crawler = $this->client->request(Request::METHOD_GET, '/register');

        $form = $crawler->selectButton('Register')->form([
            "registration_form[email]" => 'test.test@cfa-afti.fr',
            "registration_form[plainPassword]" => 'Testnum1!',
            'registration_form[agreeTerms]' => true,
            "registration_form[name]" => "TestNom",
            "registration_form[surname]" => "TestPrenom",
            "registration_form[birthDate]" => "2020-11-10T00:00:00",
            "registration_form[phoneNumber]" => "0639342934",
            "registration_form[gender]" => "M",
        ]);

        $this->client->submit($form);
        $crawler = $this->client->getCrawler();

        $this->assertEquals("You should be at least 18 years old.",
            $crawler->filter("#formRegister > div:nth-child(4) > span > span > span.form-error-message")->text());
    }
}
