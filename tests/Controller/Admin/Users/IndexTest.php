<?php


namespace App\Tests\Controller\Admin\Users;


use App\Tests\Traits\TestLoginTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexTest extends WebTestCase
{
    use TestLoginTrait;

    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    /**
     * @group admin
     */
    public function testUsersAdminUser()
    {
        $this->loginAsAdmin();
        $crawler = $this->client->request(Request::METHOD_GET, '/admin/users');

        $this->assertResponseIsSuccessful();
        $this->assertEquals("Manage users", $crawler->filter("div.col-lg-10.col-md-12.col-xs-12 > div > h3")->text());

        //$this->assertEquals("Users verified", $crawler->filter("#card-box > div > div > div:first-child > div > div > h3")->text());
    }

    /**
     * @group admin
     */
    public function testUsersCustomerUser()
    {
        $this->loginAsVerifiedUser();
        $crawler = $this->client->request(Request::METHOD_GET, '/admin/users');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    /**
     * @group admin
     */
    public function testUsersBankerUser()
    {
        $this->loginAsBanker();
        $crawler = $this->client->request(Request::METHOD_GET, '/admin/users');
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }
}
