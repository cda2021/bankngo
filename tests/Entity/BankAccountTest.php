<?php


namespace App\Tests\Entity;


use App\Entity\BankAccount;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class BankAccountTest extends TestCase
{
    /**
     * @group unit
     */
    public function testBankAccount()
    {
        $owner = new User();

        $bankAccount = new BankAccount();
        $bankAccount->setBalance(5000);
        $bankAccount->setIban("BE78311444845059");
        $bankAccount->setLabel('Livret A');
        $bankAccount->setOwner($owner);

        $this->assertEquals(5000, $bankAccount->getBalance());
        $bankAccount->addMoney(500);
        $this->assertEquals(5500, $bankAccount->getBalance());
        $bankAccount->subtractMoney(600);
        $this->assertEquals(4900, $bankAccount->getBalance());

        $this->assertEquals("BE78311444845059", $bankAccount->getIban());
        $this->assertEquals("Livret A", $bankAccount->getLabel());
    }
}
