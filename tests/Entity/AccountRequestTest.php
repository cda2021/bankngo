<?php


namespace App\Tests\Entity;


use App\Entity\AccountRequest;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AccountRequestTest extends TestCase
{
    public const fixturesDirectory = __DIR__ . "/../Fixtures/";

    /**
     * @group unit
     */
    public function testAccountRequest()
    {
        $userTransmitter = new User();
        $bankerResponder = new User();

        $declarationFile = new UploadedFile(
            self::fixturesDirectory . "declaration.pdf",
            "declaration.pdf",
            "application/pdf",
            null
        );

        $accountRequest = new AccountRequest();
        $accountRequest->setType(AccountRequest::TYPE_DELETE_ACCOUNT);
        $accountRequest->setStatus(AccountRequest::STATUS_ACCEPTED);
        $accountRequest->setUserTransmitter($userTransmitter);
        $accountRequest->setBankerResponder($bankerResponder);

        try {
            $accountRequest->setFile($declarationFile);
        } catch (\Exception $e) {
            print $e->getMessage();
        }

        $this->assertFalse($accountRequest->getOverdraftAuthorized());
        $this->assertEquals($accountRequest->getFile()->getMimeType(), "application/pdf");
        $this->assertEquals(AccountRequest::TYPE_DELETE_ACCOUNT, $accountRequest->getType());
        $this->assertInstanceOf(User::class, $accountRequest->getBankerResponder());
        $this->assertInstanceOf(User::class, $accountRequest->getUserTransmitter());
        $this->assertInstanceOf(UploadedFile::class, $accountRequest->getFile());
    }
}
