<?php


namespace App\Tests\Entity;


use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @group unit
     */
    public function testUser()
    {
        $user = new User();
        $user->setEmail("doe@bankngo.com");
        $user->setName("doe");
        $user->setSurname("john");
        $user->setRole("ROLE_ADMIN");
        $user->setBirthDate(new \DateTime("- 22 years"));
        $user->setGender(User::MALE_GENDER);
        $user->setPhoneNumber("0641607111");
        $user->setPasswordResetToken("ab32a82465fa173b36666a03273ed2ee5a8de3fcc");
        $user->setTwoFactorAuthenticationResetToken("001c5b71eda9cbed0713144557dfc011cba");


        $this->assertIsArray($user->getRoles());
        $this->assertFalse($user->isCustomer());
        $this->assertFalse($user->isVerified());
        $this->assertTrue($user->isAdmin());
        $this->assertFalse($user->getTwoFactorAuthentification());

        $this->assertEquals("Doe", $user->getName());
        $this->assertEquals("John", $user->getSurname());
        $this->assertEquals("ab32a82465fa173b36666a03273ed2ee5a8de3fcc", $user->getPasswordResetToken());
        $this->assertEquals("001c5b71eda9cbed0713144557dfc011cba", $user->getTwoFactorAuthenticationResetToken());
    }
}
