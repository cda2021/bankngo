<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201210192811 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account_request ADD closed_bank_account_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE account_request ADD overdraft_authorized BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE account_request ADD CONSTRAINT FK_F2BC9BD78FD68320 FOREIGN KEY (closed_bank_account_id) REFERENCES bank_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F2BC9BD78FD68320 ON account_request (closed_bank_account_id)');
        $this->addSql('ALTER TABLE bank_account ADD closure_account_request_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_account ADD overdraft_authorized BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE bank_account ADD overdraft DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_account ADD CONSTRAINT FK_53A23E0A2DA5BE71 FOREIGN KEY (closure_account_request_id) REFERENCES account_request (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_53A23E0A2DA5BE71 ON bank_account (closure_account_request_id)');
        $this->addSql('ALTER TABLE "user" ADD password_reset_token VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD two_factor_authentification BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD phone_number VARCHAR(15) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" DROP password_reset_token');
        $this->addSql('ALTER TABLE "user" DROP two_factor_authentification');
        $this->addSql('ALTER TABLE "user" DROP phone_number');
        $this->addSql('ALTER TABLE account_request DROP CONSTRAINT FK_F2BC9BD78FD68320');
        $this->addSql('DROP INDEX UNIQ_F2BC9BD78FD68320');
        $this->addSql('ALTER TABLE account_request DROP closed_bank_account_id');
        $this->addSql('ALTER TABLE account_request DROP overdraft_authorized');
        $this->addSql('ALTER TABLE bank_account DROP CONSTRAINT FK_53A23E0A2DA5BE71');
        $this->addSql('DROP INDEX UNIQ_53A23E0A2DA5BE71');
        $this->addSql('ALTER TABLE bank_account DROP closure_account_request_id');
        $this->addSql('ALTER TABLE bank_account DROP overdraft_authorized');
        $this->addSql('ALTER TABLE bank_account DROP overdraft');
    }
}
