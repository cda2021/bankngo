<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201214103304 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank_account ADD bic VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE bank_account ADD bank_code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE bank_account ADD agency_code VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE bank_account ADD account_number VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE bank_account ADD key INT NOT NULL');
        $this->addSql('ALTER TABLE bank_account ADD agency_address VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE bank_account DROP bic');
        $this->addSql('ALTER TABLE bank_account DROP bank_code');
        $this->addSql('ALTER TABLE bank_account DROP agency_code');
        $this->addSql('ALTER TABLE bank_account DROP account_number');
        $this->addSql('ALTER TABLE bank_account DROP key');
        $this->addSql('ALTER TABLE bank_account DROP agency_address');
    }
}
