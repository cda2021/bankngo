<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201221225718 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE account_request ADD score INT NOT NULL');
        $this->addSql('ALTER TABLE account_request ADD ocr_text TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_request ADD ocr_find_words TEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE account_request DROP score');
        $this->addSql('ALTER TABLE account_request DROP ocr_text');
        $this->addSql('ALTER TABLE account_request DROP ocr_find_words');
    }
}
