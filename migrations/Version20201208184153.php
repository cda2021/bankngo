<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201208184153 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');
        $this->addSql('CREATE TABLE account_request (id UUID NOT NULL, user_transmitter_id UUID DEFAULT NULL, banker_responder_id UUID DEFAULT NULL, status INT NOT NULL, filename VARCHAR(255) NOT NULL, original_file_name VARCHAR(255) NOT NULL, type INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F2BC9BD7DC861A49 ON account_request (user_transmitter_id)');
        $this->addSql('CREATE INDEX IDX_F2BC9BD7845CD0C1 ON account_request (banker_responder_id)');
        $this->addSql('CREATE TABLE bank_account (id UUID NOT NULL, owner_id UUID NOT NULL, balance DOUBLE PRECISION NOT NULL, iban VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_53A23E0A7E3C61F9 ON bank_account (owner_id)');
        $this->addSql('CREATE TABLE recipient (id UUID NOT NULL, transmitter_id UUID NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, iban VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6804FB49B703C510 ON recipient (transmitter_id)');
        $this->addSql('CREATE TABLE recipient_request (id UUID NOT NULL, user_transmitter_id UUID NOT NULL, banker_responder_id UUID NOT NULL, status INT NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, iban VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2DA7A79DDC861A49 ON recipient_request (user_transmitter_id)');
        $this->addSql('CREATE INDEX IDX_2DA7A79D845CD0C1 ON recipient_request (banker_responder_id)');
        $this->addSql('CREATE TABLE transfer (id UUID NOT NULL, bank_account_id UUID NOT NULL, recipient_id UUID NOT NULL, amount DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4034A3C012CB990C ON transfer (bank_account_id)');
        $this->addSql('CREATE INDEX IDX_4034A3C0E92F8F78 ON transfer (recipient_id)');
        $this->addSql('CREATE TABLE "user" (id UUID NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, birth_date DATE NOT NULL, gender VARCHAR(1) NOT NULL, active BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE account_request ADD CONSTRAINT FK_F2BC9BD7DC861A49 FOREIGN KEY (user_transmitter_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_request ADD CONSTRAINT FK_F2BC9BD7845CD0C1 FOREIGN KEY (banker_responder_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bank_account ADD CONSTRAINT FK_53A23E0A7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipient ADD CONSTRAINT FK_6804FB49B703C510 FOREIGN KEY (transmitter_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipient_request ADD CONSTRAINT FK_2DA7A79DDC861A49 FOREIGN KEY (user_transmitter_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recipient_request ADD CONSTRAINT FK_2DA7A79D845CD0C1 FOREIGN KEY (banker_responder_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transfer ADD CONSTRAINT FK_4034A3C012CB990C FOREIGN KEY (bank_account_id) REFERENCES bank_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transfer ADD CONSTRAINT FK_4034A3C0E92F8F78 FOREIGN KEY (recipient_id) REFERENCES recipient (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE transfer DROP CONSTRAINT FK_4034A3C012CB990C');
        $this->addSql('ALTER TABLE transfer DROP CONSTRAINT FK_4034A3C0E92F8F78');
        $this->addSql('ALTER TABLE account_request DROP CONSTRAINT FK_F2BC9BD7DC861A49');
        $this->addSql('ALTER TABLE account_request DROP CONSTRAINT FK_F2BC9BD7845CD0C1');
        $this->addSql('ALTER TABLE bank_account DROP CONSTRAINT FK_53A23E0A7E3C61F9');
        $this->addSql('ALTER TABLE recipient DROP CONSTRAINT FK_6804FB49B703C510');
        $this->addSql('ALTER TABLE recipient_request DROP CONSTRAINT FK_2DA7A79DDC861A49');
        $this->addSql('ALTER TABLE recipient_request DROP CONSTRAINT FK_2DA7A79D845CD0C1');
        $this->addSql('DROP TABLE account_request');
        $this->addSql('DROP TABLE bank_account');
        $this->addSql('DROP TABLE recipient');
        $this->addSql('DROP TABLE recipient_request');
        $this->addSql('DROP TABLE transfer');
        $this->addSql('DROP TABLE "user"');
    }
}
