<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201217131649 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX uniq_f2bc9bd78fd68320');
        $this->addSql('CREATE INDEX IDX_F2BC9BD78FD68320 ON account_request (closed_bank_account_id)');
        $this->addSql('ALTER TABLE bank_account DROP CONSTRAINT fk_53a23e0a2da5be71');
        $this->addSql('DROP INDEX uniq_53a23e0a2da5be71');
        $this->addSql('ALTER TABLE bank_account DROP closure_account_request_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX IDX_F2BC9BD78FD68320');
        $this->addSql('CREATE UNIQUE INDEX uniq_f2bc9bd78fd68320 ON account_request (closed_bank_account_id)');
        $this->addSql('ALTER TABLE bank_account ADD closure_account_request_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_account ADD CONSTRAINT fk_53a23e0a2da5be71 FOREIGN KEY (closure_account_request_id) REFERENCES account_request (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_53a23e0a2da5be71 ON bank_account (closure_account_request_id)');
    }
}
