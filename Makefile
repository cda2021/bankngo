up: ## launches docker
	docker-compose up

up-daemon: ## launches docker un daemon mode
	docker-compose up -d

down: ## exit docker
	docker-compose down

install:
	docker-compose exec php composer install
	docker-compose exec nodejs yarn install

test: fixtures
	docker-compose exec php php bin/phpunit

fresh-db:
	docker-compose exec php php bin/console doctrine:database:drop -f
	docker-compose exec php php bin/console doctrine:database:create
	docker-compose exec php php bin/console doctrine:migrations:migrate --no-interaction

fixtures: fresh-db
	docker-compose exec php php bin/console doctrine:fixtures:load --no-interaction

rebuild-php:
	docker-compose build php

up-mac:
	docker-compose up
	docker-compose build php

encore:
	docker-compose exec nodejs yarn dev
