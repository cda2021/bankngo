# Cahier de tests

---

## Tests fonctionnels

### Tests non-authentifié

- [x] L'utilisateur peut se connecter avec l'unique login  **5fd7734f3f048** et le mot de passe **riri**. Il devrait optenir une réponse de redirection vers le portail utilisateur (`LoginTest.php::testLogin()`)

- [x] L'utilisateur peut s'enregistrer avec de bonnes données, si ses données sont bonnes, il doit être redirigé. (`RegisterTest.php::testRegister()`)

- [x] L'utilisateur ne peut s'enregistrer s'il est trop jeune, il devrait voir sur la page le message **"You should be at least 18 years old"**** (`RegisterTest.php::testRegisterAsTooYoung()`)

### Tests authentifié en tant qu'utilisateur

- [x] L'utilisateur ne peut accéder à la route `/portal` s'il n'est pas authentifié. (`tests/Portal/IndexTest.php::testPortalNotLogged()`)

- [x] L'utilisateur peut accéder à la route `/portal` si il est authentifié et son compte validé. (`tests/Portal/IndexTest.php::testPortalLogged()`)

- [x] L'utilisateur ne peut accéder à la route `/portal` si il est authentifié et son compte n'est pas validé. (`tests/Portal/IndexTest.php::testPortalLoggedNotVerified()`)

- [x]  L'utilisateur peut modifier ses paramètres à la route `/portal/settings`. Si ses modifications sont valides, il doit voir le message **Your settings have been saved** (`tests/Portal/SettingsTest.php::testSettings()`

- [x] L'utilisateur peut voir s'il a un compte bancaire à la route `/portal`, il devrait voir **Compte courant** et s'il clique sur le lien, devrait avoir les détails de son compte. (`tests/Portal/Account/AccountTest.php::testUserHavingAccount()`)

- [x] L'utilisateur peut demander à fermer son compte bancaire . Si sa demande est valide, il devrait voir le message **Your closure account request have been received by an advisor. You will be notified by mail when it will be processed.** (`tests/Portal/Account/AccountTest.php::testUserAskingForCloseAccount()`)

- [x] L'utilisateur peut demander à ouvrir un nouveau compte bancaire. Si sa demande est valide il devrait voir **Your account request have been received by an advisor.** (`tests/Portal/Account/AccountTest.php::testUserAskingForCloseAccount()`)

### Tests authentifié en tant que banquier

- [x] L'utilisateur authentifié avec un compte portant le rôle de banquier doit pouvoir accéder à la route `/management`. (`tests/Management/ManagementTest.php::testAdminPagesWithBankerUser()`)

- [x] L'utilisateur authentifié avec un compte portant un autre rôle que banquier ne peut accéder à la route `/management`. Il recevrait une réponse **403**. (`tests/Management/ManagementTest.php::testAdminPagesWithCustomerUser()`)

- [x] Le banquier devrait pouvoir accéder à la liste des comptes bancaires en se rendant à la route `/management/accounts/` (`tests/Management/Accounts/AccountsTest.php::testAccounts()`)
- [x] Le banquier devrait pouvoir afficher les informations d'un compte bancaire en se rendant à la route `/management/accounts/{id}`. (`tests/Management/Accounts/ShowTest.php::testShowBankerUser()`)

- [x] Un utilisateur ne peut pas afficher les informations d'un compte bancaire en se rendant à la route `/management/accounts/{id}`. Il recevrait une réponse **403**.(`tests/Management/Accounts/ShowTest.php::testShowCustomerUser()`)

- [x] Un banquier peut afficher les requêtes de création/suppression de compte reçues à la route `/management/account/requests` . (`tests/Management/Requests/AccountRequestTest.php::testAccountRequestBankerUser()`)

- [x] Un utilisateur ne peut afficher les requêtes de création/suppression de compte reçues à la route `/management/account/requests` . Il recevrait une réponse **403**. (`tests/Management/Requests/AccountRequestTest.php::testShowCustomerUser()`)

- [x] Le banquier devrait pouvoir afficher les informations d'une requête en se rendant à la route `/management/account/requests/details/{id}`. (`tests/Management/Requests/Account/DetailsTest.php::testShowBankerUser()`)

- [x] Un utilisateur ne peut afficher les informations d'une requête en se rendant à la route `/management/account/requests/details/{id]` . Il recevrait une réponse **403**.  (`tests/Management/Requests/Account/DetailsTest.php::testShowBankerUser()`)

- [x] Un banquier peut accéder à ses demandes d'ajout de bénéficiaire en se rendant à la route `/management/recipient/requests` . Il pourra accepter ou refuser chacune. (`tests/Management/Accounts/ShowTest.php::testRecipientRequestBankerUser()`)

### Tests authentifié en tant qu'administrateur

- [x] En tant qu'administrateur, je devrais pouvoir accéder à la route `/admin/users`. Je devrais voir "Manage users". (`tests/Admin/Users/IndexTest.php::testUsersAdminUser()`)

- [x] En tant que banquier, je ne devrais pas pouvoir accéder à la route `/admin/users`. Je devrais voir "Manage users". (`tests/Admin/Users/IndexTest.php::testUsersBankerUser()`)

- [x] En tant qu'utilisateur, je ne devrais pas pouvoir accéder à la route `/admin/users`. Je devrais voir "Manage users". (`tests/Admin/Users/IndexTest.php::testUsersBankerUser()`)


