

## Compétence 1

- Maquetter une application
  
  - Schéma de use case
  
  - Maquette graphique (Wireframe)
  
  - Approche AGILE
  
  - Travaux avec Martha

- Développer des composants d’accès aux données
  
  - Utilisation d'un Object Relational Mapper (Doctrine)
  
  - Coder dans un langage objet, avec un style défensif
  
  - Utiliser des bibliothèques d'objets existants
  
  - Coder de façon sécurisée les accès aux données relationnelles ou non relationnelles en consultation, en création, en mise à jour et en suppression
  
  - Utiliser un outil de virtualisation ou de conteneurisation
    
    

- Développer la partie front-end d’une interface utilisateur web
  
  - Wireframe => maquette d'interface graphique
  
  - Test des interactions/traitements utilisateurs
  
  - Prise en compte des différents équipements et navigateurs ciblés
  
  - Interface conforme à la maquette
  
  - Les règles d'accessibilité sont respectées
    
    

- Développer la partie back-end d’une interface utilisateur web
  
  - Bonnes pratiques de développement PHP/Symfony
  
  - Veille technologique sur Symfony
  
  - Tests unitaires et fonctionnels
  
  - Développer dans un langage objet
  
  - Utiliser un outil collaboratif de partage de ressources
  
  - Utiliser un outil de virtualisation ou de conteneurisation
  
  - Utilisation de web services (OCR)
  
  - Gérer la sécurité de l’application (authentification, permissions…) dans la partie serveur



## Compétence 2

- Concevoir une base de données
  
  - Schéma du modèle conceptuel de données en Anglais
  
  - Rédiger les comptes rendus de réunion

- Mettre en place une base de données
  
  - Veille technologique sur le choix de la base de données
  
  - La base de données relationnelles est conforme au schéma physique

## Compétence 3

- Collaborer à la gestion d’un projet informatique et à l’organisation de l’environnement de développement
  
  - S’assurer que les documents produits en français ou en anglais respectent les règles orthographiques et grammaticales
  
  - Définir l’environnement de développement du projet => Docker
  
  - Définir un outil collaboratif de partage de ressources => Gitlab, Google Drive
  
  - Participer à la planification et au suivi du projet au sein de l'équipe de projet => Trello

- Concevoir une application
  
  - Respect du cahier des charges
  
  - Dossier de conceptualité
  
  - Use cases
  
  - Besoins de sécurité
  
  - L’architecture technique est conforme aux bonnes pratiques d’une architecture répartie sécurisée
  
  - La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre-sens, ...)
  
  - Identifier les besoins de sécurité de l’application
  
  - Adapter l’architecture technique aux besoins des utilisateurs et aux besoins de sécurité

- Développer des composants métier
  
  - Utiliser les fonctionnalités de génération de code
  
  - Coder des composants dans un langage objet, avec un style de programmation défensif
  
  - Valider la sécurité et utiliser des composants issus d'un cadre d’applications (framework) ou d'une bibliothèque => Symfony
  
  - Utiliser un outil collaboratif de partage de ressources

- Construire une application organisée en couches
  
  - Utiliser des composants tiers et en vérifier l’efficience et la sécurité

- Préparer et exécuter les plans de tests d’une application
  
  - Préparer et exécuter un plan de test de sécurité en se basant sur nos cours de sécurité des applications
  
  - Rechercher des failles de sécurité par des tests aléatoires
  
  - Pratiquer une analyse statique de l’application (PHPStan)
  
  - Rédiger le dossier de compte rendu de tests



Hors compétences : Utilisation de l'anglais à des fins de documentation technique de code / base de données / code en anglais / Application en anglais


