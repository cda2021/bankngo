# Manuel de configuration

Afin de pouvoir lancer, tester le site web de bankngo. Vous devez possèder Docker [lien téléchargement](https://www.docker.com/get-started) (veuillez choisir la version de docker desktop correspondant à votre OS).

Une fois Docker téléchargé et installé il vous faudra vous positionner à la racine du projet. Ici deux choix s'offrent à vous :

- Soit vous possédez la commande ```Make``` alors il vous suffira de faire ```make up``` qui va appeller docker avec la commande suivante ```docker-compose up```
- soit vous écrivez directement ```docker-compose up```

```docker-compose up``` fait appel au fichier docker-compose.yml et crée des ```conteneurs``` qui sont des mini linux (Docker utilise des images de Linux Alpine qui fait de base 4 mo). Ces conteneurs vont, une fois créés, lancer le serveur symfony.

```Attention, il est possible de lancer la commande docker-compose up avec l'attribut "-d" afin de l'executer en tâche de fond. Il est déconseillé de faire cela car on n'a pas de vision sur l'état des conteneurs à leur création. ```

```Cependant en lançant la commande de la sorte il faudra laisser la console utilisée ouverte car elle maintient le serveur. La supprimer stoppe les conteneurs.```

Une fois l'étape docker terminée il vous suffit de vous connecter dans votre navigateur sur l'adresse [localhost ou 127.0.0.1](https://localhost/), vous devriez voir un message vous incitant à quitter la page car le certificat SSL est auto-généré et non valide pour votre navigateur. Il faut donc passer l'avertissement pour arriver sur le site.

Normalement si toutes ces étapes se sont bien déroulées vous devriez voir la page principale du site et la configuration est terminée.
