# Sommaire

0. [Page d'Accueil](#Page-d'Accueil)

1. [Inscription](#Inscription)

2. [Double Authentification](#Double-Authentification)

3. [Identification](#Identification)

4. [Déconnexion](#Déconnexion)

5. [Portail utilisateur](#Portail-utilisateur)

6. [Paramètres utilisateur](#Parametres-utilisateur)

7. [Demande de Création & Suppression de compte Client](#Demande-de-Création-&-Suppression-de-compte-Client)

8. [Transferts](#Transferts)
   
    8.1. [Création Bénéficiaire](#Creation-Bénéficiaire)
   
    8.2. [Transferer vers un Bénéficiaire](#Transferer-vers-un-Bénéficiaire)

---

## Page d'Accueil

!["HomePage"](img/customer/0_HomePage.png)

## Inscription

Si vous ne possédez pas de compte, vous pouvez vous inscrire en cliquant sur le bouton ```Register``` en haut à droite dans la barre de navigation de la page :

!["Bouton Register"](img/customer/2_HomePage_Register.png)

Remplir les champs pour procéder à l'inscription de votre profil utilisateur bankngo.

```Prérequis obligatoires``` :

- avoir plus de 18 ans
- accepter les ```terms of service```

!["Formulaire Register"](img/customer/3_RegisterPage.png)

Si vous voulez en savoir plus sur l'option de double authentification. [Lien documentation double authentification](#Double-Authentification)

Une fois les différents champs remplis, cliquer sur le bouton ```Register``` pour valider le formulaire d'inscription.

Ensuite il vous faudra valider votre adresse mail afin de pouvoir accèder aux services du site Bancaire. Le ```lien de confirmation``` et votre ```Login``` vous seront envoyés sur l'adresse mail que vous venez de renseigner. (Voir exemple de mail ci-dessous)

!["Mail confirmation"](img/customer/4_Email_confirmation.png)

*ici le login est 5fd8d8e4cc197 et le lien de confirmation expire dans 4 heures.*

Une fois avoir cliquer sur le lien de confirmation et s'être connecté vous devriez obtenir l'interface avec le message suivants.

!["Confirmation Validated"](img/customer/5_email_confirmed.png)

*Your email address has been verified*

Félicitations, vous venez de terminer votre inscription à bankngo!

## Double Authentification

### Activer la double Authentification

!["Double Auth"](img/authenticator/Register_with_two_fact_auth.png)

Pendant l'inscription vous pouvez décider d'activer la double authentification avec la case ```Two factor authentication```, poursuivez l'[inscription](#Inscription).

!["Double Auth"](img/authenticator/Qr_authenticator.png)

Si la case à été cochée vous serez redirigés vers une pages où vous pourrez enregistrer votre clé de double authentification avec un QR code. Bankngo utilise [google authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) un des meilleurs systèmes de double validation autant pour la sécurité assurée par google et la facilité à la prise en main de l'interface.

Il faut donc scanner le code QR à l'aide de l'application et ensuite inscrire le code que donne google authenticator dans la section ```Code```. Si l'appui sur ```AUTHENTICATE``` fonctionne, vous serez connecté, sinon il y aura un message d'erreur qui vous invitera à recommencer (voir-ci dessous).

!["Error"](img/authenticator/error.png)

Une seconde manière d'activer la double authentification se situe dans [paramètres utilisateur](#Paramètres-utilisateur), il suffit de cocher la case correspondante et de sauvegarder les modifications.

### Utiliser la double authentification

Après la page Login et après avoir activé la double authentification vous vous retrouverez sur la page suivante :

!["After Login"](img/authenticator/After_login.png)

Vous devez dès à présent remplir avec le code généré par Google Authenticator.

### Désactiver la double authentification

Si vous ne pouvez plus accèder à la double authentification, vous pouvez cliquer sur ```A problem with two factor authentication?``` qui vous donne la page suivante :

!["Reset Auth"](img/authenticator/reset_Authenticator.png)

Il suffit alors d'entrer son email et vous recevrez un mail similaire à celui ci-dessous :

!["Reset email"](img/authenticator/email_reset.png)

Cliquez sur le lien et le système de double Authentification sera désactivé.

## Identification

Pour accèder au portail d'identification, veuillez cliquer sur le bouton ```Sign in```.

!["Login1"](img/customer/1_HomePage_SignIn.png)

Vous arriverez ensuite sur la page ci-dessous, vous pourrez alors entrer vos ```Login``` et ```Password``` dans les champs ci-dessous :

!["Login2"](img/customer/1_HomePage_SignIn_2.png)

## Déconnexion

Vous pouvez vous déconnecter à tout moment à l'aide du bouton ```LOG OUT``` disponible dans la barre de navigation.

!["Log Out"](img/customer/16_Logout.png)

## Portail utilisateur

Une fois [connecté](#Identification), l'interface devrait ressembler au visuel ci-dessous.

!["Portail"](img/customer/6_PortalPage.png)

## Paramètres utilisateur

Dans la catégorie ```Settings``` on retrouve quatres options que l'on peut éditer ```Two factor authentication```, ```Phone number```, ```Email``` & ```Gender```. Ces options sont modifiables à tous moments, pour enregistrer les changements appuyer sur ```SAVE```. (Visuel ci-dessous)

!["Portail"](img/customer/15_AccountSettings.png)

## Demande de Création & Suppression de compte Client

Dans la catégorie ```My accounts``` on retrouve les différents comptes possédés par le client. Si vous ne possédez pas de compte vous êtes automatiquement redirigés vers la création d'un nouveau compte(voir ci-dessous).

!["Demande acc"](img/customer/7_AccountRequest.png)

Pour faire une demande de compte vous devez choisir si vous souhaitez posséder un découvert ou non option : ```Authorized overdraft```. Et vous devez télécharger votre pièce d'identité : boutton ```Browse```, elle sera vérifiée par un banquier afin qu'il valide votre compte.

!["Demande acc"](img/customer/8_AccountRequest2.png)

Une fois le formulaire rempli envoyez avec ```SEND```.

Vous obtiendrez un message en bannière similaire à l'image ci-dessous:

!["Bannière demande acc"](img/customer/9_AccountRequest3.png)

Après vérification par le banquier, votre compte apparaîtra sur le même onglet :

!["Validé acc"](img/customer/17_AccountAfterBankerValidation.png)

En cliquant dessus vous obtiendrez plus de détails sur le compte :

!["Validé acc"](img/customer/18_AccountAfterBankerValidation2_Info.png)

Ici on aperçoit deux option ```BACK``` pour revenir à la page précédente et ```CLOSE ACCOUNT``` qui envoie une demande de fermeture de compte qui sera validée par un banquier.

## Transferts

Afin d'effectuer un transfert d'argent, vous devez d'abord ajouter un bénéficiaire.

### Création Bénéficiaire

!["Benef"](img/customer/10_BeneficiairePage.png)

Ici on constate que l'on n'a pour l'instant pas de bénéficiaire, pour en ajouter un il faut cliquer sur ```ADD A RECIPIENT```.

!["Add Benef"](img/customer/11_AjoutBeneficiaire.png)

On ce retrouve alors sur la page ci-dessus, il faut alors remplir le formulaire avec les 3 informations suivantes ```Nom```, ```Prénom```, ```IBAN```.

Pour envoyer la demande d'ajout à un banquier appuyer sur ```SEND```, sinon si vous souhaitez annuler la demande cliquez sur ```BACK```.

!["Add Benef notification"](img/customer/12_AjoutBeneficiaire_toBanker.png)

Si vous avez correctement fait votre demande le message ci-dessus devrait apparaître.

### Transferer vers un Bénéficiaire

Lorsque vous arrivez sur la section ```Transfers```, si vous n'avez pas fait de transferts avant vous ne devriez pas avoir d'historique comme ci-dessous.

!["transf"](img/customer/13_PageTransferts.png)

Après avoir cliqué sur le bouton ```SEND MONEY```, vous vous retrouverez sur la page ci-dessous.

!["transf"](img/customer/14_PageTransferts_Send.png)

Si vous avez fait une demande de bénéficiaire et quelle a été validée par un banquier, alors vous retrouverez les bénéficiaire sélectionnable dans l'option ```Recipient``` et le compte que vous voulez utiliser pour le transfert dans l'option ```Bank account```. Vous pourrez alors sélectionner le montant que vous souhaitez envoyer dans l'option ```Amount```.

!["transf"](img/customer/19_SendMoney.png)

Cliquez sur ```SEND```, félicitation, vous venez d'effectuer votre premier virement!
