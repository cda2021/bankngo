# Sommaire

0. [Page d'Accueil](#Page-d'Accueil)
1. [Identification](#Identification)
2. [Double Authentification](#Double-Authentification)
3. [Déconnexion](#Déconnexion)
4. [Portail Admin](#Portail-Admin)
5. [Parametres Admin](#Parametres-Admin)
6. [Modification d'utilisateur](#Modification-d'utilisateur)
7. [Maintenance du site](#Maintenance-du-site)
8. [historique de connection](#historique-de-connection)

---

## Page d'Accueil

!["HomePage"](img/customer/0_HomePage.png)

## Identification

Pour accèder au portail d'identification, veuillez cliquer sur le bouton ```Sign in```.

!["Login1"](img/customer/1_HomePage_SignIn.png)

Vous arriverez ensuite sur la page ci-dessous, vous pourrez alors entrer vos ```Login``` et ```Password``` dans les champs ci-dessous :

!["Login2"](img/customer/1_HomePage_SignIn_2.png)

Les banquiers sont obligés, pour des raisons de sécurité, d'utiliser la [double authentification](#Double-Authentification).

## Double Authentification

### Utiliser la double authentification

Après la page Login et après avoir activé la double authentification vous vous retrouverez sur la page suivante :

!["After Login"](img/authenticator/After_login.png)

Vous devez dès à présent remplir avec le code généré par Google Authenticator.

## Déconnexion

Vous pouvez vous déconnecter à tout moment à l'aide du bouton ```LOG OUT``` disponible dans la barre de navigation.

!["Log Out"](img/customer/16_Logout.png)

## Portail Admin

Une fois [connecté](#Identification), l'interface devrait ressembler au visuel ci-dessous.

!["Portail"](img/banker/Dashboard_banker.png)

## Parametres Admin

Dans la catégorie ```Settings``` on retrouve quatres options que l'on peut éditer ```Two factor authentication```, ```Phone number```, ```Email``` & ```Gender```. Ces options sont modifiables à tous moments, pour enregistrer les changements appuyer sur ```SAVE```. (Visuel ci-dessous)

!["Portail"](img/banker/settings.png)

## console admin

!["console"](img/admin/admin_console.png)

## Modification d'utilisateur

## Maintenance du site

L'admin peut mettre en maintenance le site.

## historique de connection

L'admin dans une version ultérieure pourra consulter les logs du site bankngo à des fins de sécurité et de contrôle