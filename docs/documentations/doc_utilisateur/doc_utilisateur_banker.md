# Sommaire

0. [Page d'Accueil](#Page-d'Accueil)
1. [Identification](#Identification)
2. [Double Authentification](#Double-Authentification)
3. [Déconnexion](#Déconnexion)
4. [Portail Banquier](#Portail-Banquier)
5. [Parametres Banquier](#Parametres-Banquier)
6. [Validation de Création & Suppression de compte Client](#Validation-de-Création-&-Suppression-de-compte-Client)
7. [Validation d'ajout de bénéficiaires](#Validation-d'ajout-de-bénéficiaires)

---

## Page d'Accueil

!["HomePage"](img/customer/0_HomePage.png)

## Identification

Pour accèder au portail d'identification, veuillez cliquer sur le bouton ```Sign in```.

!["Login1"](img/customer/1_HomePage_SignIn.png)

Vous arriverez ensuite sur la page ci-dessous, vous pourrez alors entrer vos ```Login``` et ```Password``` dans les champs ci-dessous :

!["Login2"](img/customer/1_HomePage_SignIn_2.png)

Les banquiers sont obligés, pour des raisons de sécurité, d'utiliser la [double authentification](#Double-Authentification).

## Double Authentification

### Utiliser la double authentification

Après la page Login et après avoir activé la double authentification vous vous retrouverez sur la page suivante :

!["After Login"](img/authenticator/After_login.png)

Vous devez dès à présent remplir avec le code généré par Google Authenticator.

## Déconnexion

Vous pouvez vous déconnecter à tout moment à l'aide du bouton ```LOG OUT``` disponible dans la barre de navigation.

!["Log Out"](img/customer/16_Logout.png)

## Portail Banquier

Une fois [connecté](#Identification), l'interface devrait ressembler au visuel ci-dessous.

!["Portail"](img/banker/Dashboard_banker.png)

## Parametres Banquier

Dans la catégorie ```Settings``` on retrouve quatres options que l'on peut éditer ```Two factor authentication```, ```Phone number```, ```Email``` & ```Gender```. Ces options sont modifiables à tous moments, pour enregistrer les changements appuyer sur ```SAVE```. (Visuel ci-dessous)

!["Portail"](img/banker/settings.png)

## Validation de Création & Suppression de compte Client

Dans la catégorie ```management``` on retrouve les différents comptes clients. Vous pouvez accèder à leurs détails dans Accounts(voir ci-dessous).

!["Demande acc"](img/banker/Manage_account.png)

Pour valider ou refuser une demande de compte vous devez aller dans la catégorie ```Account requests```.

!["Accounts"](img/banker/Validation_compte1.png)

Il suffit alors de choisir une demande en cliquant dessus. Apparaît alors un nouveau menu avec la demande en détail :

!["Bannière demande acc"](img/banker/Validation_compte2.png)

```ACCEPT``` pour accepter la demande ```REFUSE``` pour l'inverse, sinon si vous souhaitez garder la demande pour plus tard cliquez sur ```BACK```.

### Validation  ou refus d'ajout de bénéficiaires

Dans la catégorie ```Recipient requests``` on retrouve les différentes  demandes de bénéficiaires émises par les clients(voir ci-dessous).

!["Benef"](img/banker/Validation_beneficiare.png)

```ACCEPT``` pour accepter la demande ```REFUSE``` pour l'inverse.
