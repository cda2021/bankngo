# Documentation projet bankngo

## Sommaire

1. [Présentation du projet](#Présentation-du-projet)
2. [Fonctionnalités développées](#Fonctionnalités-développées)
3. [Développement agile](#Développement-agile)
4. [Glossaire](#Glossaire)
5. [Sources](#Sources)

---

## Présentation du projet

### résumé du projet

#### **scénario**

> Réaliser une application qui simule un portail de banque en ligne permettant d’effectuer des opérations de création de compte client, validation de la création par un banquier, consultation de compte bancaire, réalisation de virement.

#### critères d'évaluation

> Réaliser dans un langage de développement choisi par le groupe, une application sécurisée. A la remise des livrables (exécutable, code, documentation, ...), l’AFTI soumettra le code à un audit axé :
> 
> - Clarté du code
> - Maintenabilité du code
> - Sécurité du code
> 
> La méthode d’évaluation des travaux de chaque groupe ne sera connue que le jour J. Il est donc IMPORTANT que chaque groupe préserve tout au long du projet la confidentialité de son code, des failles injectées et de toute sa documentation. La divulgation de l’une de ces informations peut avoir des conséquences négatives (malus) sur la note finale du projet.

#### **exigences du cahier des charges**

#### objectifs

- faire un dossier d’architecture logiciel qui documente entre autres les choix des technologies, les choix d’architecture logicielle et de
  configuration, les bonnes pratiques de sécurité implémentées, etc.

- document spécifique sur les mesures et bonnes
  pratiques sécurité

- portail banque en ligne, création de compte client, validation par un banquier, consultation du compte bancaire, réalisation de virements.

#### **Recherches externes**

Nous nous sommes inspirés du site la finance pour tous [Lien disponible ici](https://www.lafinancepourtous.com/decryptages/marches-financiers/acteurs-de-la-finance/banque/la-banque-comment-ca-marche/) afin de mieux comprendre le fonctionnement d'un banque et donc comment nous allions articuler le projet et quelle architecture allait avoir notre site.

## executables et code sources

Pour maintenir, faire évoluer notre code nous nous sommes servis du gestionnaire de version gitlab [lien du repo](https://gitlab.com/cdabank/bankngo).

Gitlab en plus de permettre le versionning possède d'autres outils qui aides à la mise en place et à la progression des projet (au développement agile). Comme l'outil de CI et l'outil de gestion de projet(creation d'issues).

### gestion de projet

Pour faire de la gestion de projet nous nous sommes servis de deux outils :

- [trello](https://trello.com/fr)
- les [issues gitlab](https://docs.gitlab.com/ee/user/project/issues/)

## Fonctionnalités développées

#### client

- soumettre une demande de création de compte.
  
  > - formulaire d'upload piece d'identité. (Nom, prénom, date de naissance, adresse postale, adresse mail,mot de passe,)

- features :
  
  > - virements
  > - consulter son compte
  > - suppression de compte
  >   - formulaire avec upload d’une demande signée.

#### Banquier

- Valide les demandes de création et suppression de compte
- consultation des pièces jointes soumises par les clients

#### Menu non authentifié

- formulaire : page création de compte
  
  > - création est attribuée automatiquement à un des 5 banquiers (prédéfinis dans la base de données) en fonction du nombre de demande à la charge de chacun d’entre eux.

- login : suivre l'état de la demande de création, si il y en a une.
  
  > - même page de login pour les users et admins

#### Menu authentifié client

- Si compte pas validé => visualiser l'avancement de la demande

- Si compte validé => assignation d'un identifiant banquaire (voir conventions).

- Le client est alors capable
  d’ajouter des bénéficiaires, effectuer des virements, visualiser son compte
  et demander une suppression de compte.

- l'ajout de bénéficiaire doit aussi être validé par un banquier

- l'administrateur peut voir les historiques de connections

- système de double authentification pour augmenter la sécurité des utilisateurs : optionnel chez un client mais obligatoire chez un banquier ou chez l'administrateur.

#### Menu authentifié Banquier

- valider les demandes de création de compte, d'ajout de bénéficiaire et de suppression de compte.

## tests fonctionnels

Nous nous sommes servis de [phpunit](https://phpunit.de/), un framework de tests permettant de vérifier le bon fonctionnement et le comportement de l'application dans l'environnement définit.

## choix stratégiques

En ce qui concerne la vérification de la carte d'identité, nous avions imaginé un système de lecteur automatique de la carte d'identité mais faire un algo de verification nous même aurait pris beaucoup de temps même si plus sécurisé. On a choisi d'utiliser un système plus simple : verification des cartes d'identité manuelle par le banquier.

## Développement agile

!["Extreme Programming"](img/Extreme_Programming.png)

Nous avons utilisé la méthode agile extreme programming (XP)

Grands principes de la méthode XP([source](https://fr.wikipedia.org/wiki/Extreme_programming)):

- puisque la revue de code est une bonne pratique, elle sera faite en permanence (par un binôme) ; (peer programming, review back-sitting, reviews pre merge)
- puisque les tests sont utiles, ils seront faits systématiquement avant chaque mise en œuvre ; (CI gitlab, tests fonctionnels)
- puisque la conception est importante, le code sera retravaillé tout au long du projet (refactoring) ;
- puisque la simplicité permet d'avancer plus vite, la solution la plus simple sera toujours celle qui sera retenue; (choix du framework symfony, des modules connus par les développeurs au dessus de recherches)
- puisque la compréhension est importante, des métaphores seront définies et évolueront en concomitance;
- puisque l'intégration des modifications est cruciale, celles-ci seront faites plusieurs fois par jour ;(merge de plus d'une foi par jour de branches vers master)
- puisque les besoins évoluent vite, des cycles de développement très rapides faciliteront l'adaptation au changement. (cycles quotidiens)

Nous avons aussi mis en place pour l'architecture gitlab le principe de une branche = une fonctionnalité. En insistant sur l'atomicité du projet c'est-à-dire que modifier ou créer une fonctionnalité correspond à modifier le moins de fichiers possible/ on ne refait pas l'architecture à chaque commit. Et chaque commit correspond à une fonctionnalité ou un fix de bug complet et réussit.

Nous avons beaucoup utilisé le système de peer programming qui permet à 2 développeurs de s'occuper de la même fonctionnalité en utilisant seulement un seul écran. La réflexion est plus rapide et élaborée sur le développement. Cela permet aussi un partage de l'expérience de chacun sur la technologie ou les manières de développer.

## Glossaire

extreme programming:

> En informatique et plus particulièrement en génie logiciel, extreme programming (XP) est une méthode agile plus particulièrement orientée sur l'aspect réalisation d'une application, sans pour autant négliger l'aspect gestion de projet. XP est adapté aux équipes réduites avec des besoins changeants. XP pousse à l'extrême des principes simples.

Méthode Agile:

> En ingénierie logicielle, les pratiques agiles mettent en avant la collaboration entre des équipes auto-organisées et pluridisciplinaires et leurs clients. Elles s'appuient sur l'utilisation d'un cadre méthodologique léger mais suffisant centré sur l'humain et la communication. Elles préconisent une planification adaptative, un développement évolutif, une livraison précoce et une amélioration continue, et elles encouragent des réponses flexibles au changement.

Atomicité:

> L'atomicité est une propriété utilisée en programmation concurrente pour désigner une opération ou un ensemble d'opérations d'un programme qui s'exécutent entièrement sans pouvoir être interrompues avant la fin de leur déroulement. Une opération qui vérifie cette propriété est qualifiée d'« atomique », ce terme dérive de ατομος (atomos) qui signifie « que l'on ne peut diviser ».

## Sources

- wikipédia
- documentation officielle de symfony
- documentation officielle de gitlab
