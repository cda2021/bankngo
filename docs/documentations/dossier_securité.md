# Sécurité projet bankngo

## Sommaire

1. [Clarté du code & vérification statique](#Clarté-du-code-&-vérification-statique)
2. [tests fonctionnels](#tests-fonctionnels)
3. [tests unitaires](#tests-unitaires)

---

## Clarté du code & vérification statique

Utilisation de PHPStan, outil d'analyse statique du code.

[lien de phpstan sur github](https://github.com/phpstan/phpstan)

Si le code passe une vérification statique cela permettra :

- d'enlever les variable inutilisées
- enlever les failles possible dues aux bugs syntaxiques

## tests fonctionnels

Chacune des fonctionnalités possibles à un utilisateur doivent être testées fonctionnellement. Grâce à PHPUnit et de son Bridge avec Symfony (une extension permettant d'utiliser PHPUnit pour tester le framework), nous pouvons nous rendre sur des routes et tester ce qui s'affiche dans chaque page, ou tester une intéraction utilisateur ne réquierant pas de javascript (soumission de formulaire, navigation...).


### CI gitlab

la CI gitlab fonctionne de manière simple on upload une image docker de notre serveur et la CI s'occupe de tester si nos commit envoyés sur une branche protègée par la CI fonctionne. Le code est mis à jour sur le serveur gitlab et ce dernier le lit et tente de l'executer/interprèter. Si le changement se passe mal alors la CI tag une erreur sur le commit, on peut alors avoir un rapport détaillé sur le commit et ses erreurs.

### jetons csrf ou  ANTI Cross-Site Request Forger

L'attaque CSRF est un type d'attaque qui incite la victime à effectuer la tâche malveillante sur une application Web authentifiée par la victime au nom des intérêts des attaquants. L'attaquant utilise l'authentification acquise dans la session en cours de la victime pour effectuer la tâche malveillante. C'est la raison pour laquelle cette attaque est également appelée Session Riding. L'attaque CSRF exploitera le concept selon lequel si l'utilisateur est authentifié. L'attaquant exploitera ce concept en identifiant le cookie de session de la session et l'utilisera pour envoyer données utile à exécuter sur l'application.

Un token CSRF est unique, secret et possède une valeur non prévisible qui est générée côté serveur et transmise vers le client de manière à ce que il utilise le contexte HTTP de ce dernier. Quand la requête est faite le côté serveur de l'application valide ou invalide la requête en fonction de la validité et de la correspondance du token communiqué.

Les tokens CSRF empêchent les attaques CSRF d'avoir lieu en rendant impossible pour un attaquant de construire une requête HTTP complète simulant la victime. Comme l'attaquant n'est pas en mesure d'anticiper ou de prédire la valeur du token CSRF de l'utilisateur, il ne peut pas remplir tous les paramètres nécessaires pour que la requête HTTP soit validée.

### Double Authentification

La double authentification, authentification à deux facteurs (A2F), authentification à double facteur ou vérification en deux étapes (two-factor authentication en anglais, ou 2FA) est une méthode d'authentification forte par laquelle un utilisateur peut accéder à une ressource informatique (un ordinateur, un téléphone intelligent ou encore un site web) après avoir présenté deux preuves d'identité distinctes à un mécanisme d'authentification. Un exemple de ce processus est l'accès à un compte bancaire grâce à un guichet automatique bancaire : seule la combinaison de la carte bancaire (que l'usager détient) et du numéro d'identification personnel (que l'usager connaît) permet de consulter le solde du compte et de retirer de l'argent.
