<?php

namespace App\Security;

use App\Domain\EventSubscriber\TwoFactorAuthenticationSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class EmailVerifier
{
    /** @var VerifyEmailHelperInterface  */
    private $verifyEmailHelper;

    /** @var MailerInterface  */
    private $mailer;

    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var TokenStorageInterface  */
    private $tokenStorage;

    /** @var SessionInterface  */
    private $session;

    public function __construct(
        VerifyEmailHelperInterface $helper,
        MailerInterface $mailer,
        EntityManagerInterface $manager,
        TokenStorageInterface $tokenStorage,
        SessionInterface $session
    ) {
        $this->verifyEmailHelper = $helper;
        $this->mailer = $mailer;
        $this->entityManager = $manager;
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
    }

    /**
     * @param string $verifyEmailRouteName
     * @param UserInterface $user
     * @param TemplatedEmail $email
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendEmailConfirmation(string $verifyEmailRouteName, UserInterface $user, TemplatedEmail $email): void
    {
        $signatureComponents = $this->verifyEmailHelper->generateSignature(
            $verifyEmailRouteName,
            $user->getId(),
            $user->getEmail()
        );

        $context = $email->getContext();
        $context['signedUrl'] = $signatureComponents->getSignedUrl();
        $context['expiresAt'] = $signatureComponents->getExpiresAt();
        $context['uniqueLogin'] = $user->getUniqueLogin();

        $email->context($context);

        $this->mailer->send($email);
    }

    /**
     * @param Request $request
     * @param UserInterface $user
     * @throws VerifyEmailExceptionInterface
     */
    public function handleEmailConfirmation(Request $request, UserInterface $user): void
    {
        $this->verifyEmailHelper->validateEmailConfirmation($request->getUri(), $user->getId(), $user->getEmail());

        $user->setIsVerified(true);
        $user->setRole("ROLE_CUSTOMER");

        /** @var PostAuthenticationGuardToken $currentToken */
        $currentToken = $this->tokenStorage->getToken();

        $roles = array_merge($currentToken->getRoleNames(), ["ROLE_CUSTOMER"]);
        $newToken = new PostAuthenticationGuardToken($currentToken->getUser(), $currentToken->getProviderKey(), $roles);
        $this->tokenStorage->setToken($newToken);
        $this->session->set('_security_' . $currentToken->getProviderKey(), serialize($newToken));

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
