<?php


namespace App\Domain\EventSubscriber;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

/**
 * Class TwoFactorAuthenticationSubscriber
 * @package App\Domain\EventSubscriber
 */
class TwoFactorAuthenticationSubscriber implements EventSubscriberInterface
{
    const ROLE_2FA_SUCCEED = "2FA_SUCCEED";
    const FIREWALL_NAME = "main";
    const ROUTE_FOR_2FA = "two-factor";
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage, RouterInterface $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', -10],
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if (in_array($event->getRequest()->attributes->get('_route'),
            ["app_login", self::ROUTE_FOR_2FA, "reset_two_factor", "reset_two_factor_validation", "app_verify_email"])) {
            return;
        }

        if (($currentToken = $this->tokenStorage->getToken()) && $currentToken instanceof PostAuthenticationGuardToken) {
            if ($currentToken->getProviderKey() === self::FIREWALL_NAME) {

                if ($this->twoFactorDisabled($currentToken)) {
                    return;
                }

                if (!$this->hasRole($currentToken, self::ROLE_2FA_SUCCEED)) {
                    $response = new RedirectResponse($this->router->generate(self::ROUTE_FOR_2FA));
                    $event->setResponse($response);
                }
            }
        }
    }

    /**
     * @param TokenInterface $token
     * @param string $role
     * @return bool
     */
    private function hasRole(TokenInterface $token, string $role): bool
    {
        foreach ($token->getRoleNames() as $userRole) {
            if ($userRole === $role) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param TokenInterface|null $currentToken
     * @return bool
     */
    private function twoFactorDisabled(?TokenInterface $currentToken)
    {
        $user = $currentToken->getUser();

        if (false === $user->getTwoFactorAuthentification()) {
            return true;
        }

        return false;
    }
}
