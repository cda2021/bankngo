<?php


namespace App\Domain\Services\RecipientRequest;


use App\Entity\User;

/**
 * Interface RequestRecipientServiceInterface
 * @package App\Domain\Services\RecipientRequest
 */
interface RequestRecipientServiceInterface
{
    public function findBankerResponder(): User;
}
