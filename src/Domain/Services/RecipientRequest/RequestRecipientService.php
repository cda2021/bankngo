<?php


namespace App\Domain\Services\RecipientRequest;


use App\Entity\RecipientRequest;
use App\Entity\User;
use App\Repository\RecipientRequestRepository;
use App\Repository\UserRepository;

class RequestRecipientService implements RequestRecipientServiceInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var RecipientRequestRepository
     */
    private $requestRepository;

    /**
     * RequestRecipientService constructor.
     * @param UserRepository $userRepository
     * @param RecipientRequestRepository $requestRepository
     */
    public function __construct(UserRepository $userRepository, RecipientRequestRepository $requestRepository)
    {
        $this->userRepository = $userRepository;
        $this->requestRepository = $requestRepository;
    }

    /**
     * methods that finds a banker and link it to a recipient request
     * @return User
     */
    public function findBankerResponder(): User
    {
        $bankers = $this->userRepository->findBy(["role" => "ROLE_BANKER"]);

        $datas = [];

        foreach ($bankers as $key => $banker) {
            $numberOfRequests = $this->requestRepository->count(["bankerResponder" => $banker, "status" => RecipientRequest::STATUS_IN_PROGRESS]);
            $datas[$key] = ["numberOfRequests" => $numberOfRequests, "banker" => $banker];
        }

        $selectedBanker = null;
        $min = 999999999;

        foreach ($datas as $data) {
            if ($data['numberOfRequests'] < $min) {
                $min = $data['numberOfRequests'];
                $selectedBanker = $data["banker"];
            }
        }

        if ($selectedBanker !== null) {
            return $selectedBanker;
        }

        return $this->userRepository->findOneBy(["role" => "ROLE_BANKER"]);
    }
}
