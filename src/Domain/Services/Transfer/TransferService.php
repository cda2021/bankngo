<?php


namespace App\Domain\Services\Transfer;


use App\Entity\Transfer;
use App\Repository\BankAccountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use function Symfony\Component\String\b;

/**
 * Class TransferService
 * @package App\Domain\Services\Transfer
 */
class TransferService implements TransferServiceInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var BankAccountRepository
     */
    private $bankAccountRepository;

    /**
     * TransferService constructor.
     * @param EntityManagerInterface $entityManager
     * @param BankAccountRepository $bankAccountRepository
     */
    public function __construct(EntityManagerInterface $entityManager, BankAccountRepository $bankAccountRepository)
    {
        $this->entityManager = $entityManager;
        $this->bankAccountRepository = $bankAccountRepository;
    }

    /**
     * @param Transfer $transfer
     * @return bool
     * We check if the amount of the transfer can be took of the account
     */
    public function isTransferPossible(Transfer $transfer): bool
    {
        $bankAccount = $transfer->getBankAccount();
        $actualBalance = $bankAccount->getBalance();

        if ($transfer->getAmount() <= $actualBalance) {
            return true;
        }

        return false;
    }

    /**
     * transfert the value from a bank account to another
     * @param Transfer $transfer
     * @return bool
     */
    public function makeTransfer(Transfer $transfer): bool
    {
        if ($this->isTransferPossible($transfer)) {
            $bankAccount = $transfer->getBankAccount();
            $bankAccount->subtractMoney($transfer->getAmount());

            $this->sendMoneyToAccountIfExist($transfer);

            $this->entityManager->persist($transfer);
            $this->entityManager->persist($bankAccount);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    /**
     * If we found the targetted account in our bank change the value of the reciever
     * @param Transfer $transfer
     */
    private function sendMoneyToAccountIfExist(Transfer $transfer)
    {
        $bankAccount = $this->bankAccountRepository->findOneBy(["iban" => $transfer->getRecipient()->getIban()]);

        if ($bankAccount) {
            $bankAccount->addMoney($transfer->getAmount());
            $this->entityManager->persist($bankAccount);
        }
    }
}
