<?php


namespace App\Domain\Services\Transfer;


use App\Entity\Transfer;

/**
 * Interface TransferServiceInterface
 * @package App\Domain\Services\Transfer
 */
interface TransferServiceInterface
{
    public function isTransferPossible(Transfer $transfer): bool;

    public function makeTransfer(Transfer $transfer): bool;
}
