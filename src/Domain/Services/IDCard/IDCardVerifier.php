<?php


namespace App\Domain\Services\IDCard;


use App\Entity\AccountRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class IDCardVerifier implements IDCardVerifierInterface
{
    const DEFAULT_FRENCH_ID_CARD_CONTENT = [
        "REPUBLIQUE",
        "FRANCAISE",
        "NATIONALITE",
        "FRANCE",
        "CARTE",
        "FRA",
        "REP",
        "SEXE",
        "NE",
        "IDENTITE"
    ];

    private $filesFolder;
    /**
     * @var HttpClientInterface
     */
    private $httpClient;
    /**
     * @var string
     */
    private $ocrApiUrl;
    /**
     * @var string
     */
    private $ocrApiKey;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        string $filesFolder,
        string $ocrApiUrl,
        string $ocrApiKey,
        HttpClientInterface $httpClient,
        EntityManagerInterface $entityManager
    )
    {
        $this->filesFolder = $filesFolder;
        $this->httpClient = $httpClient;
        $this->ocrApiUrl = $ocrApiUrl;
        $this->ocrApiKey = $ocrApiKey;
        $this->entityManager = $entityManager;
    }

    /**
     * @param AccountRequest $accountRequest
     * @throws TransportExceptionInterface
     */
    public function verify(AccountRequest $accountRequest): void
    {
        $data = [
            "file" => DataPart::fromPath($this->filesFolder . $accountRequest->getFilename()),
            "apikey" => $this->ocrApiKey
        ];

        $formData = new FormDataPart($data);
        $response = $this->httpClient->request(Request::METHOD_POST, $this->ocrApiUrl, [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body' => $formData->bodyToString()
        ]);

        $content = $this->decodeResponse($response);

        $this->parseContent($accountRequest, $content);
    }

    private function decodeResponse(ResponseInterface $response): ?string
    {
        $content = json_decode($response->getContent(), true);
        $text = $content["ParsedResults"][0]["ParsedText"];

        return preg_replace('~[\r\n]+~', " ", $text);
    }

    private function parseContent(AccountRequest $accountRequest, ?string $content): void
    {
        $content = $this->removeAccents($content);
        $content = strtoupper($content);
        $score = 0;
        $findWords = [];
        $transmitter = $accountRequest->getUserTransmitter();

        $searchedWords = array_merge(self::DEFAULT_FRENCH_ID_CARD_CONTENT, [
            strtoupper($transmitter->getName()),
            strtoupper($transmitter->getSurname()),
            $transmitter->getBirthDate()->format("d.m.Y")
        ]);

        foreach ($searchedWords as $searchedWord) {
            if (preg_match("/{$searchedWord}/i", $content)) {
                $score++;
                $findWords[] = $searchedWord;
            }
        }

        $this->saveResults($accountRequest, $content, json_encode($findWords), $score);
    }

    private function removeAccents(string $text): string
    {
        return strtr(utf8_decode($text), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'),'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

    private function saveResults(AccountRequest $accountRequest, string $content, string $findWords, int $score): void
    {
        $accountRequest->setScore($score);
        $accountRequest->setOcrText($content);
        $accountRequest->setOcrFindWords($findWords);

        $this->entityManager->flush();
    }
}
