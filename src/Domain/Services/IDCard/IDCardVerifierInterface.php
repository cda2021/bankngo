<?php


namespace App\Domain\Services\IDCard;


use App\Entity\AccountRequest;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

interface IDCardVerifierInterface
{
    /**
     * @param AccountRequest $accountRequest
     * @throws TransportExceptionInterface
     */
    public function verify(AccountRequest $accountRequest): void;
}
