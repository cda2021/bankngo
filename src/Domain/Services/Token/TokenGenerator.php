<?php


namespace App\Domain\Services\Token;


class TokenGenerator implements TokenGeneratorInterface
{

    /**
     * method that generates our csrf tokens
     * @return string
     * @throws \Exception
     */
    public function generate(): string
    {
        $length = random_int(20, 35);
        $bytes = random_bytes($length);
        $token = bin2hex($bytes);

        return $token;
    }
}
