<?php


namespace App\Domain\Services\Token;

/**
 * Interface TokenGeneratorInterface
 * @package App\Domain\Services\Token
 */
interface TokenGeneratorInterface
{
    public function generate(): string;
}
