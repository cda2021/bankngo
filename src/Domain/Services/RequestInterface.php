<?php


namespace App\Domain\Services;


use App\Entity\User;

/**
 * Interface RequestInterface
 * @package App\Domain\Services
 */
interface RequestInterface
{
    public function findBankerResponder(): User;
}
