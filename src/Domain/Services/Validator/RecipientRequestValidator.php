<?php


namespace App\Domain\Services\Validator;


use App\Domain\Services\Mail\MailerServiceInterface;
use App\Domain\Services\Notification\UserRealTimeNotifier;
use App\Entity\Recipient;
use App\Entity\RecipientRequest;
use Doctrine\ORM\EntityManagerInterface;

class RecipientRequestValidator
{
    /**
     * @var MailerServiceInterface
     */
    private $mailerService;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRealTimeNotifier
     */
    private $notifier;

    public function __construct(
        EntityManagerInterface $entityManager,
        MailerServiceInterface $mailerService,
        UserRealTimeNotifier $notifier
    ) {
        $this->entityManager = $entityManager;
        $this->mailerService = $mailerService;
        $this->notifier = $notifier;
    }

    public function validate(int $status, RecipientRequest $recipientRequest): void
    {
        $notificationText = null;
        $recipientRequest->setStatus($status);

        if($status === RecipientRequest::STATUS_ACCEPTED) {
            $recipient = new Recipient();
            $recipient->setIban($recipientRequest->getIban());
            $recipient->setName($recipientRequest->getName());
            $recipient->setSurname($recipientRequest->getSurname());
            $recipient->setTransmitter($recipientRequest->getUserTransmitter());
            $this->entityManager->persist($recipient);

            $notificationText = "The recipient MR/MRS {$recipient->getName()} {$recipient->getSurname()} has been added to your recipient list.";
            $this->mailerService->acceptMessageNewRecipient($recipientRequest->getUserTransmitter()->getEmail(), $recipientRequest->getName());
        }

        if ($status === RecipientRequest::STATUS_REJECTED) {
            $notificationText = "Your recipient request ({$recipientRequest->getName()}) have been rejected by your banker. Please check your portal.";
            $this->mailerService->refuseMessageNewRecipient($recipientRequest->getUserTransmitter()->getEmail(), $recipientRequest->getName());
        }

        if (null !== $notificationText) {
            $this->notifier->notify($recipientRequest->getUserTransmitter(), $notificationText);
        }

        $this->entityManager->flush();
    }
}
