<?php


namespace App\Domain\Services\Validator;


use App\Domain\Services\Mail\MailerServiceInterface;
use App\Domain\Services\Notification\UserRealTimeNotifier;
use App\Entity\AccountRequest;
use App\Entity\BankAccount;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;

class AccountRequestValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var MailerServiceInterface
     */
    private $mailService;
    /**
     * @var UserRealTimeNotifier
     */
    private $notifier;

    public function __construct(EntityManagerInterface $entityManager, MailerServiceInterface $mailService, UserRealTimeNotifier $notifier)
    {
        $this->entityManager = $entityManager;
        $this->mailService = $mailService;
        $this->notifier = $notifier;
    }

    public function validate(int $status, int $type, AccountRequest $accountRequest)
    {
        $accountRequest->setStatus($status);

        if($status === AccountRequest::STATUS_ACCEPTED) {
            $this->acceptAccountRequest($type, $accountRequest);
        } else {
            $this->denyAccountRequest($type, $accountRequest);
        }

        $this->entityManager->flush();
    }

    private function acceptAccountRequest(int $type, AccountRequest $accountRequest)
    {
        switch ($type) {
            case AccountRequest::TYPE_NEW_ACCOUNT:
                $faker = Factory::create();
                $bankAccount = new BankAccount();
                $bankAccount->setIban($faker->iban());
                $bankAccount->setBalance(0);
                $bankAccount->setOwner($accountRequest->getUserTransmitter());
                $bankAccount->setLabel("Compte Courant");
                $this->entityManager->persist($bankAccount);

                //mail confirm account
                $notificationText = "Your account opening request have been accepted. Please check your portal.";
                $this->notifier->notify($accountRequest->getUserTransmitter(), $notificationText);
                $this->mailService->acceptMessageNewAccount($accountRequest->getUserTransmitter()->getEmail());
                break;

            case AccountRequest::TYPE_DELETE_ACCOUNT:
                $bankAccount = $accountRequest->getClosedBankAccount();
                $bankAccount->setActive(false);

                //mail close account
                $notificationText = "Your account closure request have been accepted and your account #{$bankAccount->getAccountNumber()} have been closed. Please check your portal.";
                $this->notifier->notify($accountRequest->getUserTransmitter(), $notificationText);
                $this->mailService->acceptMessageCloseAccount($accountRequest->getUserTransmitter()->getEmail(), $bankAccount->getAccountNumber());
                break;
        }
    }

    private function denyAccountRequest(int $type, AccountRequest $accountRequest)
    {
        switch ($type) {
            case AccountRequest::TYPE_NEW_ACCOUNT:
                $notificationText = "Your account opening request have been refused. Please check your portal.";
                $this->notifier->notify($accountRequest->getUserTransmitter(), $notificationText);
                $this->mailService->refusedMessageNewAccount($accountRequest->getUserTransmitter()->getEmail());
                break;

            case AccountRequest::TYPE_DELETE_ACCOUNT:
                $notificationText = "Your account closure request have been refused. Please check your portal.";
                $this->notifier->notify($accountRequest->getUserTransmitter(), $notificationText);
                $this->mailService->refusedMessageCloseAccount($accountRequest->getUserTransmitter()->getEmail(), $accountRequest->getClosedBankAccount()->getAccountNumber());
                break;
        }
    }
}
