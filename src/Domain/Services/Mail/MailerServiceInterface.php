<?php


namespace App\Domain\Services\Mail;


use App\Entity\User;

/**
 * Interface MailerServiceInterface
 * @package App\Domain\Services\Mail
 */
interface MailerServiceInterface
{

    public function sendMail($mailTo, $subject, $text): void;

    public function confirmMessageRequestNewAccount($mailTo, $bankerName, $bankerSurname): void;

    public function confirmMessageRequestCloseAccount($mailTo): void;

    public function refuseMessageNewAccount($mailTo): void;

    public function acceptMessageNewAccount($mailTo): void;

    public function acceptMessageCloseAccount($mailTo, $numberAccount): void;

    public function refusedMessageNewAccount($mailTo): void;

    public function refusedMessageCloseAccount($mailTo, $numberAccount): void;

    public function confirmMessageRequestNewRecipient($mailTo): void;

    public function acceptMessageNewRecipient($mailTo, $recipient): void;

    public function refuseMessageNewRecipient($mailTo, $recipient): void;

    public function confirmMessageSavedSettings($mailTo): void;

    public function resetTwoFactorAuthenticator(User $user): void;
}
