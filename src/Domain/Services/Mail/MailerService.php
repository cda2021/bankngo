<?php

namespace App\Domain\Services\Mail;

use App\Entity\User;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailerService implements MailerServiceInterface
{
    public const MAIL_BANKNGO = "bankngo@gmail.com";


    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * MailerService constructor.
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * function that send mail to a specified mail address with a subject and a text
     * @param $mailTo
     * @param $subject
     * @param $text
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendMail($mailTo, $subject, $text): void
    {

        $email = (new TemplatedEmail())
            ->from(self::MAIL_BANKNGO)
            ->to($mailTo)
            ->subject($subject)
            ->text($text)
            ->htmlTemplate('mail/messageMail.html.twig')
            ->context([
                'title' => $subject,
                'message' => $text
            ]);

        $this->mailer->send($email);

    }

    /**
     * function that contains mail content that is send when a new account request is made
     * @param $mailTo
     * @param $bankerName
     * @param $bankerSurname
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function confirmMessageRequestNewAccount($mailTo, $bankerName, $bankerSurname): void
    {
        $subject = "Request New Account";
        $text = "Your request to add a new account one has been registered! It will be validated by a banker soon.\n You are assigned to $bankerSurname $bankerName.";

        $this->sendMail($mailTo, $subject, $text);
    }

    public function confirmMessageRequestCloseAccount($mailTo): void
    {
        $subject = "Request Close Account";
        $text = "Your request to close account one has been registered! It will be validated by a banker soon.";

        $this->sendMail($mailTo, $subject, $text);
    }

    /**
     * function that contains mail content that is send when a new account is refused
     * @param $mailTo
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function refuseMessageNewAccount($mailTo): void
    {
        $subject = "Nouveau compte refusé";
        $text = "Votre demande pour ajouter un nouveau à bien été refuser !";

        $this->sendMail($mailTo, $subject, $text);
    }

    /**
     * function that contains mail content that is send when a new account is validated
     * @param $mailTo
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function acceptMessageNewAccount($mailTo): void
    {
        $subject = "New account validated";
        $text = "Your request to add a new account has been accepted!";

        $this->sendMail($mailTo, $subject, $text);
    }

    public function acceptMessageCloseAccount($mailTo, $numberAccount): void
    {
        $subject = "Close account validated";
        $text = "Your account number " . $numberAccount . " have been close!";

        $this->sendMail($mailTo, $subject, $text);
    }

    /**
     * function that resets the two factor authenticator
     * @param User $user
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function refusedMessageNewAccount($mailTo): void
    {
        $subject = "New account refused";
        $text = "Your request to add a new account has been refused!";

        $this->sendMail($mailTo, $subject, $text);
    }

    public function refusedMessageCloseAccount($mailTo, $numberAccount): void
    {
        $subject = "Close account refused";
        $text = "Your request to close account ( " . $numberAccount . "has been refused!";

        $this->sendMail($mailTo, $subject, $text);
    }

    //Mail Recipient
    public function confirmMessageRequestNewRecipient($mailTo): void
    {
        $subject = "Request New Recipient";
        $text = "Your request to add a new recipient one has been registered! It will be validated by a banker soon.";

        $this->sendMail($mailTo, $subject, $text);
    }

    public function acceptMessageNewRecipient($mailTo, $recipient): void
    {
        $subject = "New Recipient accepted";
        $text = "Your recipient " . $recipient . " has been accepted";

        $this->sendMail($mailTo, $subject, $text);
    }

    public function refuseMessageNewRecipient($mailTo, $recipient): void
    {
        $subject = "New Recipient refused";
        $text = "Your recipient " . $recipient . " has been refused";

        $this->sendMail($mailTo, $subject, $text);
    }


    //Mail Settings
    public function confirmMessageSavedSettings($mailTo): void
    {
        $subject = "Saved settings";
        $text = "Your settings have been saved.";

        $this->sendMail($mailTo, $subject, $text);
    }

    //Mail TwoFactor
    public function resetTwoFactorAuthenticator(User $user): void
    {
        $email = (new TemplatedEmail())
            ->from(self::MAIL_BANKNGO)
            ->to($user->getEmail())
            ->subject("Reset Two Factor Authentication")
            ->htmlTemplate('mail/two-factor-reset.html.twig')
            ->context([
                'user' => $user
            ]);

        $this->mailer->send($email);
    }

}
