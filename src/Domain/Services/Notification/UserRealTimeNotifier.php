<?php


namespace App\Domain\Services\Notification;


use App\Entity\User;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Serializer\SerializerInterface;

class UserRealTimeNotifier
{
    /**
     * @var PublisherInterface
     */
    private $publisher;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(PublisherInterface $publisher, SerializerInterface $serializer)
    {
        $this->publisher = $publisher;
        $this->serializer = $serializer;
    }

    public function notify(User $user, string $message, array $options = []): void
    {
        $data = $this->serializer->serialize([
            "message" => $message,
            "timestamp" => (new \DateTime())->getTimestamp()
        ], "json");

        $update = new Update("http://notifications.bankngo.com/notify/{$user->getId()}", $data, true);

        $this->publisher->__invoke($update);
    }
}
