<?php


namespace App\Domain\Services\AccountRequest;


use App\Entity\User;

/**
 * Interface RequestAccountServiceInterface
 * @package App\Domain\Services\AccountRequest
 */
interface RequestAccountServiceInterface
{
    public function findBankerResponder(): User;
}
