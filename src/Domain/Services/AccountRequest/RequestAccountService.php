<?php


namespace App\Domain\Services\AccountRequest;


use App\Entity\AccountRequest;
use App\Entity\User;
use App\Repository\AccountRequestRepository;
use App\Repository\UserRepository;

class RequestAccountService implements RequestAccountServiceInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var AccountRequestRepository
     */
    private $requestRepository;

    /**
     * RequestAccountService constructor.
     * @param UserRepository $userRepository
     * @param AccountRequestRepository $requestRepository
     */
    public function __construct(UserRepository $userRepository, AccountRequestRepository $requestRepository)
    {
        $this->userRepository = $userRepository;
        $this->requestRepository = $requestRepository;
    }

    /**
     * function that finds the banker that corresponds to the request
     * @return User
     */
    public function findBankerResponder(): User
    {

        $bankers = $this->userRepository->findBy(["role" => "ROLE_BANKER"]);

        $datas = [];

        foreach ($bankers as $key => $banker) {
            $numberOfRequests = $this->requestRepository->count(["bankerResponder" => $banker, "status" => AccountRequest::STATUS_IN_PROGRESS]);
            $datas[$key] = ["numberOfRequests" => $numberOfRequests, "banker" => $banker];
        }

        $selectedBanker = null;
        $min = 999999999;

        foreach ($datas as $data) {
            if ($data['numberOfRequests'] < $min) {
                $min = $data['numberOfRequests'];
                $selectedBanker = $data["banker"];
            }
        }

        if ($selectedBanker !== null) {
            return $selectedBanker;
        }

        return $this->userRepository->findOneBy(["role" => "ROLE_BANKER"]);
    }
}
