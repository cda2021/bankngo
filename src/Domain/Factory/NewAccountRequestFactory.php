<?php


namespace App\Domain\Factory;


use App\Entity\AccountRequest;
use App\Entity\User;

/**
 * Class NewAccountRequestFactory
 * @package App\Domain\Factory
 */
class NewAccountRequestFactory
{
    /**
     * function that create the request for a new request in order to open an account
     * @param User $transmitter
     * @return AccountRequest
     */
    public static function createAccountRequest(User $transmitter): AccountRequest
    {
        $accountRequest = new AccountRequest();
        $accountRequest->setType(AccountRequest::TYPE_NEW_ACCOUNT);
        $accountRequest->setStatus(AccountRequest::STATUS_IN_PROGRESS);
        $accountRequest->setUserTransmitter($transmitter);

        return $accountRequest;
    }
}
