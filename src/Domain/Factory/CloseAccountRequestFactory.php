<?php


namespace App\Domain\Factory;


use App\Entity\AccountRequest;
use App\Entity\BankAccount;
use App\Entity\User;

/**
 * Class CloseAccountRequestFactory
 * @package App\Domain\Factory
 */
class CloseAccountRequestFactory
{
    /**
     * function that create the request for a new request for closing account
     * @param User $transmitter
     * @param BankAccount $bankAccount
     * @return AccountRequest
     */
    public static function createAccountRequest(User $transmitter, BankAccount $bankAccount): AccountRequest
    {
        $accountRequest = new AccountRequest();
        $accountRequest->setType(AccountRequest::TYPE_DELETE_ACCOUNT);
        $accountRequest->setStatus(AccountRequest::STATUS_IN_PROGRESS);
        $accountRequest->setUserTransmitter($transmitter);
        $accountRequest->setClosedBankAccount($bankAccount);

        return $accountRequest;
    }
}
