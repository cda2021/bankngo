<?php


namespace App\Controller;

use App\Entity\User;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\WebLink\Link;

class DiscoverAction extends AbstractController
{
    /**
     * @Route("/discover", name="discover", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user && $user->isVerified()) {
            $this->addLink($request, new Link('mercure', "https://localhost:443/.well-known/mercure"));

            $key = Key\InMemory::plainText('!ChangeMe!'); // don't forget to set this parameter! Test value: !ChangeMe!
            $configuration = Configuration::forSymmetricSigner(new Sha256(), $key);

            $token = $configuration->builder()
                ->withClaim('mercure', ['subscribe' => ["http://notifications.bankngo.com/notify/{$user->getId()}"]]) // can also be a URI template, or *
                ->getToken($configuration->signer(), $configuration->signingKey())
                ->toString();

            $response = $this->json(['success' => true, 'topic' => "http://notifications.bankngo.com/notify/{$user->getId()}"]);
            $cookie = new Cookie('mercureAuthorization', $token, 0, "/.well-known/mercure", null, true, true, 'strict');
            $response->headers->setCookie($cookie);

            return $response;
        }

        $response = $this->json(['success' => false], Response::HTTP_UNAUTHORIZED);

        return $response;
    }
}
