<?php


namespace App\Controller\Reset;


use App\Domain\Services\Mail\MailerService;
use App\Domain\Services\Token\TokenGeneratorInterface;
use App\Form\ResetType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TwoAuthenticationAction
 * @package App\Controller\Reset
 */
class TwoAuthenticationAction extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $repository;
    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;
    /**
     * @var MailerService
     */
    private $mailer;

    public function __construct(UserRepository $repository, TokenGeneratorInterface $tokenGenerator, MailerService $mailerService)
    {
        $this->repository = $repository;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailerService;
    }

    /**
     * Allows user who haves issues with 2FA to reset it for his account.
     * @Route("/two-factor/reset", name="reset_two_factor", methods={"GET","POST"})
     */
    public function __invoke(Request $request)
    {
        $form = $this->createForm(ResetType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get("email")->getData();
            $user = $this->repository->findOneBy(["email" => $email]);

            if ($user) {
                $token = $this->tokenGenerator->generate();
                $user->setTwoFactorAuthenticationResetToken($token);
                $this->mailer->resetTwoFactorAuthenticator($user);

                $this->getDoctrine()->getManager()->flush();
            }

            $this->addFlash("success", "Please check your email address to reset the 2Factor Authentication.");
        }

        return $this->render("reset/two-factor.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
