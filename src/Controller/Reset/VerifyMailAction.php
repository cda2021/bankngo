<?php


namespace App\Controller\Reset;


use App\Domain\Services\Mail\MailerService;
use App\Entity\User;
use App\Form\ResetType;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VerifyMailAction
 * @package App\Controller\Reset
 */
class VerifyMailAction extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $repository;
    /**
     * @var MailerService
     */
    private $mailer;
    /**
     * @var EmailVerifier
     */
    private $emailVerifier;

    public function __construct(UserRepository $repository, MailerService $mailerService, EmailVerifier $emailVerifier)
    {
        $this->repository = $repository;
        $this->mailer = $mailerService;
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * Allows user who haves issues with the verification mail to resend it.
     * @Route("/verifiy-mail/reset", name="reset_verify_mail", methods={"GET","POST"})
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Exception
     */
    public function __invoke(Request $request)
    {
        $form = $this->createForm(ResetType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get("email")->getData();
            $user = $this->repository->findOneBy(["email" => $email]);

            if ($user) {
                $lastEmailAt = $user->getLastVerifyEmailResetAt();

                if ($user->isVerified()) {
                    $this->addFlash("fail", "You are already verified.");

                    return $this->redirectToRoute("app_login");
                }

                if ($lastEmailAt) {
                    // Checks if user have already sent a request in the last 3 hours.

                    $now = new \DateTime('now');
                    $interval = date_diff($lastEmailAt, $now);

                    if ($interval->d > 0) {
                        $this->sendMail($user);
                    } elseif ($interval->h >= 3) {
                        $this->sendMail($user);
                    } else {
                        $this->addFlash("fail", "You have already asked for re-send the mail.");

                        return $this->redirectToRoute("home");
                    }
                } else {
                    $this->sendMail($user);
                }
            }

            $this->addFlash("success", "Please check your email address.");

            return $this->redirectToRoute("home");
        }

        return $this->render("reset/verify-mail.html.twig", [
            "form" => $form->createView()
        ]);
    }

    private function sendMail(User $user)
    {
        try {
            $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                (new TemplatedEmail())
                    ->from(new Address('no-reply@bankngo.com', 'bankngo-bot'))
                    ->to($user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );

            $user->setLastVerifyEmailResetAt(new \DateTime("now"));
            $this->getDoctrine()->getManager()->flush();
        } catch (TransportExceptionInterface $e) {
            $this->addFlash("fail", "An error has occured. Please retry.");
        }
    }
}
