<?php

namespace App\Controller\Reset\Validate;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TwoAuthenticationValidationAction
 * @package App\Controller\Reset\Validate
 */
class TwoAuthenticationValidationAction extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Validation to the reset URL sent in the mail to reset 2fa authentication
     * @Route("/two-factor/reset/validate/{email}/{token}/", name="reset_two_factor_validation", methods={"GET"})
     */
    public function __invoke(Request $request, $email, $token)
    {
        $user = $this->repository->findOneBy(["email" => $email, "twoFactorAuthenticationResetToken" => $token]);

        if ($user) {
            $user->setGoogleAuthenticatorSecret(null);
            $user->setTwoFactorAuthentification(false);
            $user->setTwoFactorAuthenticationResetToken(null);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "The Two Factor Authentication have been reset. Please login.");

            return $this->redirectToRoute("app_login");
        }

        $this->addFlash("fail", "Invalid token. Please retry.");

        return $this->redirectToRoute("reset_two_factor");
    }
}
