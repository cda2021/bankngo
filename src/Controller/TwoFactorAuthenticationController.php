<?php


namespace App\Controller;


use App\Domain\EventSubscriber\TwoFactorAuthenticationSubscriber;
use App\Entity\User;
use App\Form\GoogleAuthenticatorType;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Doctrine\ORM\EntityManagerInterface;
use PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException;
use PragmaRX\Google2FA\Exceptions\InvalidCharactersException;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;
use PragmaRX\Google2FA\Google2FA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

/**
 * Class TwoFactorAuthenticationController
 * @package App\Controller
 */
class TwoFactorAuthenticationController extends AbstractController
{

    /**
     * Two Factor Authenticator form / QR Code generation / Handle validation
     * @Route("/two-factor", name="two-factor", methods={"GET", "POST"})
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|Response
     * @throws IncompatibleWithGoogleAuthenticatorException
     * @throws InvalidCharactersException
     * @throws SecretKeyTooShortException
     */
    public function twoFactorAction(Request $request, TokenStorageInterface $tokenStorage, SessionInterface $session,
                                    EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(GoogleAuthenticatorType::class);
        $google2fa = new Google2FA();
        $svg = null;
        /** @var User $user */
        $user = $this->getUser();

        $form->handleRequest($request);

        if (!$user->getGoogleAuthenticatorSecret()) {
            if ($session->get('2fa_secret')) {
                $secret = $session->get('2fa_secret');
            } else {
                $secret = $google2fa->generateSecretKey();
                $request->getSession()->set('2fa_secret', $secret);
            }

            $svg = $this->generateSvgForUser($google2fa, $user, $secret);
        } else {
            $secret = $user->getGoogleAuthenticatorSecret();
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $code = $form->getData()["code"];
            $codeIsValid = $google2fa->verifyKey($secret, $code, 4);

            if ($codeIsValid) {
                if (!$user->getGoogleAuthenticatorSecret()) {
                    $user->setGoogleAuthenticatorSecret($secret);
                    $entityManager->persist($user);
                    $entityManager->flush();
                }

                $this->addRoleTwoFA($tokenStorage, $session);

                return $this->redirectToRoute("home");
            }

            $this->addFlash("fail", "Invalid verification code");
        }

        return $this->render("security/two-factor.html.twig", [
            "svg" => $svg,
            "form" => $form->createView(),
        ]);
    }

    private function generateSvgForUser(Google2FA $google2fa, User $user, string $secret)
    {
        $google2faURL = $google2fa->getQRCodeUrl(
            "BANK N GO",
            $user->getName() . " " . $user->getUsername(),
            $secret
        );

        $writer = new Writer(
            new ImageRenderer(
                new RendererStyle(400),
                new SvgImageBackEnd()
            )
        );

        return $writer->writeString($google2faURL);
    }

    private function addRoleTwoFA(TokenStorageInterface $tokenStorage, SessionInterface $session)
    {
        /** @var PostAuthenticationGuardToken $currentToken */
        $currentToken = $tokenStorage->getToken();

        $roles = array_merge($currentToken->getRoleNames(), [TwoFactorAuthenticationSubscriber::ROLE_2FA_SUCCEED]);
        $newToken = new PostAuthenticationGuardToken($currentToken->getUser(), $currentToken->getProviderKey(), $roles);
        $tokenStorage->setToken($newToken);
        $session->set('_security_' . $currentToken->getProviderKey(), serialize($newToken));
    }
}
