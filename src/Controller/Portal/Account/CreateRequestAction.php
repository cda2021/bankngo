<?php


namespace App\Controller\Portal\Account;


use App\Domain\Factory\NewAccountRequestFactory;
use App\Domain\Services\AccountRequest\RequestAccountServiceInterface;
use App\Domain\Services\IDCard\IDCardVerifierInterface;
use App\Domain\Services\Mail\MailerServiceInterface;
use App\Entity\AccountRequest;
use App\Entity\User;
use App\Form\AccountRequestType;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class CreateRequestAction
 * @package App\Controller\Portal\Account
 */
class CreateRequestAction extends AbstractController
{
    /**
     * @var RequestAccountServiceInterface
     */
    private $service;
    /**
     * @var MailerServiceInterface
     */
    private $mailService;
    /**
     * @var IDCardVerifierInterface
     */
    private $cardVerifier;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        RequestAccountServiceInterface $service,
        MailerServiceInterface $mailerService,
        IDCardVerifierInterface $cardVerifier,
        LoggerInterface $logger
    )
    {
        $this->service = $service;
        $this->mailService = $mailerService;
        $this->cardVerifier = $cardVerifier;
        $this->logger = $logger;
    }

    /**
     * New account request form
     * @param Request $request
     * @Route("/portal/request/new", name="portal_new_account_request", methods={"GET", "POST"})
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $accountRequest = NewAccountRequestFactory::createAccountRequest($user);

        $form = $this->createForm(AccountRequestType::class, $accountRequest, [
            'type' => AccountRequest::TYPE_NEW_ACCOUNT
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $banker = $this->service->findBankerResponder();
            $accountRequest->setBankerResponder($banker);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($accountRequest);
            $manager->flush();

            try {
                $this->cardVerifier->verify($accountRequest);
            } catch (TransportExceptionInterface $exception) {
                $this->logger->warning("An error has occurred with the card verifier service : {$exception->getMessage()}");
            }

            $this->addFlash("success", "Your account request have been received by an advisor.");

            //mail confirm account
            $this->mailService->confirmMessageRequestNewAccount($user->getEmail(), $banker->getName(), $banker->getSurname());

            return $this->redirectToRoute("portal");
        }

        return $this->render("portal/account/create_account_request.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
