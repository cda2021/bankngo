<?php


namespace App\Controller\Portal\Account;


use App\Domain\Factory\CloseAccountRequestFactory;
use App\Domain\Services\AccountRequest\RequestAccountServiceInterface;
use App\Domain\Services\Mail\MailerServiceInterface;
use App\Entity\AccountRequest;
use App\Entity\User;
use App\Form\AccountRequestType;
use App\Repository\BankAccountRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ClosureRequestAction
 * @package App\Controller\Portal\Account
 */
class ClosureRequestAction extends AbstractController
{

    /**
     * @var RequestAccountServiceInterface
     */
    private $service;
    /**
     * @var BankAccountRepository
     */
    private $accountRepository;

    /**
     * @var MailerServiceInterface
     */
    private $mailService;

    public function __construct(RequestAccountServiceInterface $service, BankAccountRepository $accountRepository, MailerServiceInterface $mailerService)
    {
        $this->service = $service;
        $this->accountRepository = $accountRepository;
        $this->mailService = $mailerService;
    }

    /**
     * @param Request $request
     * @Route("/portal/request/close/{id}", name="portal_remove_account_request", methods={"GET", "POST"})
     * @return RedirectResponse|Response
     */
    public function __invoke(Request $request, string $id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $account = $this->accountRepository->findOneBy(['owner' => $user, 'id' => $id]);
        $accountRequest = CloseAccountRequestFactory::createAccountRequest($user, $account);

        $form = $this->createForm(AccountRequestType::class, $accountRequest, [
            'type' => AccountRequest::TYPE_DELETE_ACCOUNT
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $banker = $this->service->findBankerResponder();
            $accountRequest->setBankerResponder($banker);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($accountRequest);
            $manager->flush();

            $this->addFlash("success", "Your closure account request have been received by an advisor. You will be notified by mail when it will be processed.");

            //mail confirm account
            $this->mailService->confirmMessageRequestCloseAccount($user->getEmail());

            return $this->redirectToRoute("portal");
        }

        return $this->render("portal/account/close_account_request.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
