<?php

namespace App\Controller\Portal\Account;


use App\Repository\BankAccountRepository;
use App\Repository\TransferRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountDetailsAction
 * @package App\Controller\Portal\Account
 */
class AccountDetailsAction extends AbstractController
{
    /**
     * Details of an user's account.
     * @Route("/portal/account/details/{id}", name="account_details")
     * @param string $id
     * @param BankAccountRepository $bankRepository
     * @param TransferRepository $transferRepository
     * @return Response
     */
    public function __invoke(string $id, BankAccountRepository $bankRepository, TransferRepository $transferRepository): Response
    {
        $account = $bankRepository->findOneBy(['id' => $id, 'owner' => $this->getUser()->getId()]);
        $transfers = $transferRepository->findBy(['bankAccount' => $account->getId()]);

        return $this->render("portal/account/details.html.twig", [
            'transfers' => $transfers,
            'account' => $account
        ]);
    }
}
