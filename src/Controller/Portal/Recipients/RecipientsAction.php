<?php


namespace App\Controller\Portal\Recipients;


use App\Entity\User;
use App\Repository\RecipientRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RecipientsAction
 * @package App\Controller\Portal\Recipients
 */
class RecipientsAction extends AbstractController
{

    private $recipientRequestRepository;

    public function __construct(RecipientRequestRepository $recipientRequestRepository)
    {
        $this->recipientRequestRepository = $recipientRequestRepository;
    }

    /**
     * @return Response
     * @Route("/portal/recipients/", name="portal_recipients", methods={"GET"})
     */
    public function __invoke(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $recipientRequests = $this->recipientRequestRepository->findBy(['userTransmitter' => $user], ['status' => 'ASC', 'createdAt' => 'DESC']);

        return $this->render('portal/recipients/index.html.twig', [
            'recipients' => $user->getRecipients(),
            'recipientRequests' => $recipientRequests
        ]);
    }
}
