<?php


namespace App\Controller\Portal\Recipients;


use App\Domain\Services\Mail\MailerServiceInterface;
use App\Domain\Services\RecipientRequest\RequestRecipientServiceInterface;
use App\Entity\RecipientRequest;
use App\Entity\User;
use App\Form\RecipientRequestType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AddRecipientsAction
 * @package App\Controller\Portal\Recipients
 */
class AddRecipientsAction extends AbstractController
{
    /**
     * @var RequestRecipientServiceInterface
     */
    private $service;

    /**
     * @var MailerServiceInterface
     */
    private $mailService;

    public function __construct(RequestRecipientServiceInterface $service, MailerServiceInterface $mailerService)
    {
        $this->service = $service;
        $this->mailService = $mailerService;
    }

    /**
     * Add new recipient.
     * @Route("/portal/recipients/add", name="portal_add_recipients", methods={"GET","POST"})
     */
    public function __invoke(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $recipientRequest = new RecipientRequest();
        $recipientRequest->setStatus(RecipientRequest::STATUS_IN_PROGRESS);
        $recipientRequest->setUserTransmitter($user);

        $form = $this->createForm(RecipientRequestType::class, $recipientRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $recipientRequest->setBankerResponder($this->service->findBankerResponder());

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($recipientRequest);
            $manager->flush();

            $this->addFlash("success", "Your recipient request have been received by an advisor.");

            //mail confirm account
            $this->mailService->confirmMessageRequestNewRecipient($user->getEmail());

            return $this->redirectToRoute("portal_recipients");
        }

        return $this->render("portal/recipients/create.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
