<?php


namespace App\Controller\Portal\Recipients;


use App\Entity\Recipient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RemoveRecipientsAction
 * @package App\Controller\Portal\Recipients
 */
class RemoveRecipientsAction extends AbstractController
{
    /**
     * @Route("/portal/recipients/remove/{id}", name="portal_remove_recipients", methods={"DELETE"})
     */
    public function __invoke(Request $request, Recipient $recipient)
    {
        if ($this->isCsrfTokenValid("validate".$recipient->getId(), $request->request->get("_token")))
        {
            $this->addFlash("success",
                "The recipient {$recipient->getSurname()} {$recipient->getName()} has been removed from your recipient list.");
            $this->getDoctrine()->getManager()->remove($recipient);
            $this->getDoctrine()->getManager()->flush();

        }

        return $this->redirectToRoute("portal_recipients");
    }
}
