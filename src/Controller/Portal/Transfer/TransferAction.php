<?php


namespace App\Controller\Portal\Transfer;


use App\Entity\User;
use App\Repository\TransferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TransferAction
 * @package App\Controller\Portal\Transfer
 */
class TransferAction extends AbstractController
{
    /**
     * @var TransferRepository
     */
    private $transferRepository;

    public function __construct(TransferRepository $repository)
    {
        $this->transferRepository = $repository;
    }

    /**
     * @Route("/portal/transfer/", name="portal_transfer", methods={"GET"})
     */
    public function __invoke()
    {
        /** @var User $user */
        $user = $this->getUser();
        $transfers = $this->transferRepository->findByUser($user);

        return $this->render("portal/transfer/index.html.twig", [
            "transfers" => $transfers
        ]);
    }
}
