<?php


namespace App\Controller\Portal\Transfer;


use App\Domain\Services\Transfer\TransferServiceInterface;
use App\Entity\Transfer;
use App\Form\TransferType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreateAction
 * @package App\Controller\Portal\Transfer
 */
class CreateAction extends AbstractController
{
    /**
     * @var TransferServiceInterface
     */
    private $transferService;

    public function __construct(TransferServiceInterface $transferService)
    {
        $this->transferService = $transferService;
    }

    /**
     * @Route("/portal/transfer/new", name="portal_create_transfer", methods={"GET", "POST"})
     */
    public function __invoke(Request $request)
    {
        $transfer = new Transfer();
        $form = $this->createForm(TransferType::class, $transfer, [
            "user" => $this->getUser()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->transferService->makeTransfer($transfer)) {
                $this->addFlash("success", "Your transfer have been solved.");

                return $this->redirectToRoute("portal_recipients");
            }

            $this->addFlash("fail", "Your balance have not enough money to transfer this amount.");

            return $this->redirectToRoute("portal_create_transfer");
        }

        return $this->render("portal/transfer/create.html.twig", [
            "form" => $form->createView(),
            ""
        ]);
    }
}
