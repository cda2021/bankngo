<?php

namespace App\Controller\Portal;


use App\Repository\AccountRequestRepository;
use App\Repository\BankAccountRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PortalAction
 * @package App\Controller\Portal
 */
class PortalAction extends AbstractController
{
    private $accountRequestRepository;
    private $bankAccountRepository;

    public function __construct(AccountRequestRepository $accountRequestRepository, BankAccountRepository $bankAccountRepository)
    {
        $this->accountRequestRepository = $accountRequestRepository;
        $this->bankAccountRepository = $bankAccountRepository;
    }

    /**
     * Portal page
     * @Route("/portal", name="portal", methods={"GET"})
     * @return Response
     */
    public function __invoke(): Response
    {
        $user = $this->getUser();
        $accounts = $this->bankAccountRepository->findBy(['owner' => $user, 'active' => true]);

        $accountRequests = $this->accountRequestRepository->findBy(['userTransmitter' => $user], ['createdAt' => 'DESC']);

        return $this->render("portal/index.html.twig", [
            'accounts' => $accounts,
            'accountRequests' => $accountRequests
        ]);
    }
}
