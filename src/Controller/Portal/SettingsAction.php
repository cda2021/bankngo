<?php


namespace App\Controller\Portal;

use App\Domain\Services\Mail\MailerServiceInterface;
use App\Form\UserSettingsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SettingsAction extends AbstractController
{
    /**
     * Settings page for user
     * @var MailerServiceInterface
     */
    private $mailService;

    public function __construct(MailerServiceInterface $mailerService)
    {
        $this->mailService = $mailerService;
    }

    /**
     * @Route("/portal/settings", name="portal_settings", methods={"GET","POST"})
     */
    public function __invoke(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserSettingsType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("success", "Your settings have been saved.");

            //mail confirm account
            $this->mailService->confirmMessageSavedSettings($user->getEmail());

            return $this->redirectToRoute('portal_settings');
        }
        return $this->render("portal/settings/index.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
