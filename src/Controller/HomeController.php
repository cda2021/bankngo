<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function home(): Response
    {
        return $this->render('pages/index.html.twig');
    }
    /**
     * @Route("/faq", name="faq", methods={"GET"})
     */
    public function faq(): Response
    {
        return $this->render('pages/faq.html.twig');
    }
}
