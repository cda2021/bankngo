<?php

namespace App\Controller\Management;

use App\Entity\AccountRequest;
use App\Entity\User;
use App\Repository\AccountRequestRepository;
use App\Repository\BankAccountRepository;
use App\Repository\RecipientRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ManagementAction extends AbstractController
{
    private $bankAccountRepository;
    private $accountRequestRepository;
    private $recipientRequestRepository;

    public function __construct(BankAccountRepository $bankAccountRepository, AccountRequestRepository $accountRequestRepository, RecipientRequestRepository $recipientRequestRepository)
    {
        $this->bankAccountRepository = $bankAccountRepository;
        $this->accountRequestRepository = $accountRequestRepository;
        $this->recipientRequestRepository = $recipientRequestRepository;
    }

    /**
     * Home page for the banker connected.
     * @Route("/management", name="management")
     */
    public function __invoke(): Response
    {
        $currentUser = $this->getUser();
        $notifAccounts = $this->bankAccountRepository->count(["active" => true]);
        $notifAccountsRequest = $this->accountRequestRepository->count(["status" => AccountRequest::STATUS_IN_PROGRESS, 'bankerResponder' => $currentUser]);
        $notifRecipientRequest = $this->recipientRequestRepository->count(["status" => AccountRequest::STATUS_IN_PROGRESS, 'bankerResponder' => $currentUser]);


        return $this->render('management/index.html.twig', [
            "notifAccounts" => $notifAccounts,
            "notifAccountsRequest" => $notifAccountsRequest,
            "notifRecipientRequest" => $notifRecipientRequest
        ]);

    }
}
