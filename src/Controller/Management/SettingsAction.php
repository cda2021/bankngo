<?php


namespace App\Controller\Management;


use App\Form\UserSettingsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SettingsAction
 * @package App\Controller\Management
 */
class SettingsAction extends AbstractController
{
    /**
     * Allows to the banker to edit his settings.
     * @Route("/management/settings", name="management_settings", methods={"GET","POST"})
     */
    public function __invoke(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserSettingsType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("success", "Your settings have been saved.");

            return $this->redirectToRoute('management_settings');
        }
        return $this->render("management/settings/index.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
