<?php


namespace App\Controller\Management\Accounts;


use App\Entity\BankAccount;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShowAction
 * @package App\Controller\Management\Accounts
 */
class ShowAction extends AbstractController
{
    /**
     * GET an account id and shows all data about it.
     * @Route("/management/accounts/{id}", name="management_account_show", methods={"GET"})
     * @param BankAccount $bankAccount
     * @return Response
     */
    public function __invoke(BankAccount $bankAccount): Response
    {
        return $this->render("management/accounts/show.html.twig", [
            "account" => $bankAccount
        ]);
    }
}
