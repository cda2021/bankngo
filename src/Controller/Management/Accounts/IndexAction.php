<?php


namespace App\Controller\Management\Accounts;


use App\Repository\BankAccountRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexAction
 * @package App\Controller\Management\Accounts
 */
class IndexAction extends AbstractController
{
    /**
     * @var BankAccountRepository
     */
    private $repository;

    public function __construct(BankAccountRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Shows all bank accounts for the banker
     * @Route("/management/accounts/", name="management_account", methods={"GET"})
     */
    public function __invoke(): Response
    {
        $accounts = $this->repository->findBy(["active" => true]);

        return $this->render("management/accounts/index.html.twig", [
            "accounts" => $accounts
        ]);
    }
}
