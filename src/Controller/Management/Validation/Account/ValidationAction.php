<?php

namespace App\Controller\Management\Validation\Account;

use App\Domain\Services\Validator\AccountRequestValidator;
use App\Entity\AccountRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ValidationAction extends AbstractController
{
    /**
     * POST Received with CSRF protection to
    /**
     * @var AccountRequestValidator
     */
    private $requestValidator;

    public function __construct(AccountRequestValidator $requestValidator)
    {
        $this->requestValidator = $requestValidator;
    }

    /**
     * @Route("/management/account/validation/{id}", name="banker_account_validation_action", methods={"POST"})
     */
    public function __invoke(Request $request, AccountRequest $accountRequest): Response
    {
        if ($this->isCsrfTokenValid("validate".$accountRequest->getId(), $request->request->get("_token")))
        {
            $type = intval($request->request->get("type"));
            $status = intval($request->request->get("validate"));

            $this->requestValidator->validate($status, $type, $accountRequest);

        }
        return $this->redirectToRoute("management_account_requests");

    }
}
