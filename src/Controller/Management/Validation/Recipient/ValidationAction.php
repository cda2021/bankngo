<?php

namespace App\Controller\Management\Validation\Recipient;

use App\Domain\Services\Mail\MailerServiceInterface;
use App\Domain\Services\Validator\RecipientRequestValidator;
use App\Entity\Recipient;
use App\Entity\RecipientRequest;
use Faker\Factory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BankerRecipientValidationAction
 *  Gets a recipient request et sets if it's accepted or not with constants.
 * @package App\Controller\Management\Validation\Recipient
 */
class ValidationAction extends AbstractController
{
    /**
     * @var RecipientRequestValidator
     */
    private $validator;

    public function __construct(RecipientRequestValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @Route("/management/recipient/validation/{id}", name="banker_recipient_validation_action", methods={"POST"})
     * @param Request $request
     * @param RecipientRequest $recipientRequest
     * @return Response
     */
    public function __invoke(Request $request, RecipientRequest $recipientRequest): Response
    {
        if ($this->isCsrfTokenValid("validate".$recipientRequest->getId(), $request->request->get("_token")))
        {
            $status = intval($request->request->get("validate"));
            $this->validator->validate($status, $recipientRequest);
        }

        return $this->redirectToRoute("management_recipient_requests");
    }
}
