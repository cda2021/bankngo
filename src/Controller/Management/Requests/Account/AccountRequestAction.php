<?php

namespace App\Controller\Management\Requests\Account;

use App\Entity\AccountRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountRequestAction extends AbstractController
{
    /**
     * @Route("/management/account/requests", name="management_account_requests", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $currentUser = $this->getUser();
        $repository = $this->getDoctrine()->getRepository(AccountRequest::class);
        $requests = $repository->findBy(['status'=> AccountRequest::STATUS_IN_PROGRESS, 'bankerResponder' => $currentUser]);

        return $this->render('management/requests/account/index.html.twig', [
            'requests' => $requests,
        ]);
    }
}
