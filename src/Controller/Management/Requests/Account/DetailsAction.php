<?php

namespace App\Controller\Management\Requests\Account;

use App\Repository\AccountRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DetailsAction extends AbstractController
{
    /**
     * @Route("/management/account/requests/details/{id}", name="management_account_requests_details", methods={"GET"})
     * @param string $id
     * @param AccountRequestRepository $accountRequestRepository
     * @return Response
     */
    public function __invoke(string $id, AccountRequestRepository $accountRequestRepository): Response
    {
        $request = $accountRequestRepository->findOneBy(['id' => $id]);

        return $this->render('management/requests/account/show.html.twig', [
            'request' => $request,
        ]);
    }
}
