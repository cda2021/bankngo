<?php


namespace App\Controller\Management\Requests\Account;


use App\Repository\AccountRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OpenFileAction
 * @package App\Controller\Management\Requests\Account
 */
class OpenFileAction extends AbstractController
{
    /**
     * @Route("/management/account/requests/details/{id}/file", name="management_account_requests_details_file", methods={"GET"})
     * @param string $id
     * @param AccountRequestRepository $accountRequestRepository
     * @param $filesFolder
     * @return BinaryFileResponse
     */
    public function __invoke(string $id, AccountRequestRepository $accountRequestRepository, $filesFolder): Response
    {
        $request = $accountRequestRepository->findOneBy(['id' => $id]);

        return new BinaryFileResponse($filesFolder.$request->getFilename());
    }
}
