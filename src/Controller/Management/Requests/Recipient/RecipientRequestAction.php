<?php

namespace App\Controller\Management\Requests\Recipient;

use App\Entity\RecipientRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RecipientRequestAction
 * @package App\Controller\Management\Requests\Recipient
 */
class RecipientRequestAction extends AbstractController
{
    /**
     * @Route("/management/recipient/requests", name="management_recipient_requests", methods={"GET"})
     */
    public function __invoke(Request $request): Response
    {
        $currentUser = $this->getUser();
        $repository = $this->getDoctrine()->getRepository(RecipientRequest::class);
        $requests = $repository->findBy(array('status'=> RecipientRequest::STATUS_IN_PROGRESS,
                                              'bankerResponder' => $currentUser));

        return $this->render('management/requests/recipient/index.html.twig', [
            'requests' => $requests,
        ]);
    }
}
