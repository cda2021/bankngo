<?php

namespace App\Controller\Admin\Users;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UsersAction
 * @package App\Controller\Admin\Users
 */
class UsersAction extends AbstractController
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * List all users registered.
     * @Route("/admin/users", name="admin_users", methods={"GET"})
     */
    public function __invoke(): Response
    {
        $users = $this->repository->findAll();

        return $this->render('admin/users/index.html.twig', [
            "users" => $users
        ]);

    }
}
