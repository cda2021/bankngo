<?php

namespace App\Controller\Admin\Users;

use App\Entity\User;
use App\Form\EditUserFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class EditAction
 * @package App\Controller\Admin\Users
 */
class EditAction extends AbstractController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * GET the user id from the URL and show an edit form for it.
     * @Route("/admin/users/edit/{id}", name="admin_user_edit", methods={"GET", "POST"})
     */
    public function __invoke(Request $request, User $user): Response
    {
        $form = $this->createForm(EditUserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $plainPassword = $form->get('plainPassword')->getData();
            $reset2FA = $form->get('resetTwoFactorAuthentication')->getData();

            if (null !== $plainPassword) {
                $user->setPassword($this->passwordEncoder->encodePassword($user, $plainPassword));
            }

            if (true === $reset2FA) {
                $user->setGoogleAuthenticatorSecret(null);
                $user->setTwoFactorAuthentification(false);
                $user->setTwoFactorAuthenticationResetToken(null);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/users/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
