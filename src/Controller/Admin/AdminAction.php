<?php

namespace App\Controller\Admin;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminAction
 * @package App\Controller\Admin
 */
class AdminAction extends AbstractController
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/admin", name="admin", methods={"GET"})
     * @return Response
     */
    public function __invoke(): Response
    {
        $verifiedUsers = $this->userRepository->count(["isVerified" => true, "role" => "ROLE_CUSTOMER"]);
        $notVerifiedUsers = $this->userRepository->count(["isVerified" => false]);
        $bankers = $this->userRepository->count(["role" => "ROLE_BANKER"]);
        $usersWith2FA = $this->userRepository->count(["twoFactorAuthentification" => true]);

        return $this->render('admin/index.html.twig', [
            "verifiedUsers" => $verifiedUsers,
            "notVerifiedUsers" => $notVerifiedUsers,
            "bankers" => $bankers,
            "usersWith2FA" => $usersWith2FA
        ]);

    }
}
