<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class EditUserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Password',
                'required' => false
            ])
            ->add('name')
            ->add('surname')
            ->add('uniqueLogin')
            ->add('birthDate', BirthdayType::class, [
                'widget' => 'single_text',
                'label' => 'Birth date'
            ])
            ->add('gender', ChoiceType::class, [
                'choices'  => [
                    'Male' => User::MALE_GENDER,
                    'Female' => User::FEMALE_GENDER,
                ],
            ])
            ->add('active')
            ->add('isVerified')
            ->add('resetTwoFactorAuthentication', CheckboxType::class, [
                "mapped" => false,
                "label" => "Reset 2FA Authentication",
                "required" => false
            ])
            ->add("role", ChoiceType::class, [
                "choices" => [
                    "ADMIN" => "ROLE_ADMIN",
                    "BANKER" => "ROLE_BANKER",
                    "CUSTOMER" => "ROLE_CUSTOMER",
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
