<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class GoogleAuthenticatorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add("code", TextType::class, [
            "label" => false,
            "attr" => [
                "placeholder" => "6 numbers code"
            ],
            'constraints' => [
                new NotBlank([
                    'message' => 'Please enter a code.',
                ]),
                new Length([
                    'min' => 6,
                    'max' => 6,
                    'exactMessage' => 'Your code should be {{ limit }} characters long.',
                ])
            ],
        ])
        ;
    }
}
