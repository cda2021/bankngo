<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'attr' => [
                    'placeholder' => "Email"
                ]
            ])
            ->add('gender', ChoiceType::class, [
                'choices'  => [
                    'Male' => User::MALE_GENDER,
                    'Female' => User::FEMALE_GENDER,
                ]
            ])
            ->add('twoFactorAuthentification')
            ->add('phoneNumber', TelType::class, [
                "required" => false,
                'attr' => [
                    'placeholder' => "Phone number"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
