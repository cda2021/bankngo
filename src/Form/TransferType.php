<?php

namespace App\Form;

use App\Entity\BankAccount;
use App\Entity\Recipient;
use App\Entity\Transfer;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];

        $builder
            ->add('recipient', EntityType::class, [
                'class' => Recipient::class,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder("r")
                        ->andWhere("r.transmitter = :user")
                        ->setParameter("user", $user)
                        ->orderBy("r.createdAt", "ASC");
                },
                'choice_label' => 'iban'
            ])
            ->add('amount', NumberType::class, [
                'input' => 'string'
            ])
            ->add('bankAccount', EntityType::class, [
                'class' => BankAccount::class,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder("b")
                        ->andWhere("b.owner = :user")
                        ->setParameter("user", $user)
                        ->orderBy("b.createdAt", "ASC");
                },
                'choice_label' => 'label'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transfer::class,
        ]);
        $resolver->setRequired("user");
    }
}
