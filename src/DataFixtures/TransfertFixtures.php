<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Transfer;

class TransfertFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        # creation of a new User object
        $transfert = new Transfer();
        $transfert->setRecipient($this->getReference("recipient1"));
        $transfert->setAmount(random_int(50, 600));
        $transfert->setBankAccount($this->getReference("accUser1"));

        # persits data into db
        $manager->persist($transfert);
        $manager->flush();

    }
    public function getDependencies()
    {
        return [
            RecipientFixtures::class,
            BankAccountFixtures::class
        ];
    }
}
