<?php

namespace App\DataFixtures;


use App\Entity\RecipientRequest;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class RecipientRequestFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $recipientRequest = new RecipientRequest();
        $recipientRequest->setUserTransmitter($this->getReference("user1"));
        $recipientRequest->setBankerResponder($this->getReference("banker"));
        $recipientRequest->setStatus(RecipientRequest::STATUS_IN_PROGRESS);
        $recipientRequest->setName($faker->lastName);
        $recipientRequest->setSurname($faker->firstName);
        $recipientRequest->setIban($faker->iban());

        $manager->persist($recipientRequest);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
