<?php

namespace App\DataFixtures;


use App\Entity\AccountRequest;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AccountRequestFixtures extends Fixture implements DependentFixtureInterface
{
    private const FIXTURES_DIRECTORY = __DIR__ . "/../../fixtures/";
    private $filesFolder;

    public function __construct($filesFolder)
    {
        $this->filesFolder = $filesFolder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $accountRequestRiri = new AccountRequest();
        $accountRequestRiri->setType(AccountRequest::TYPE_NEW_ACCOUNT);
        $accountRequestRiri->setStatus(AccountRequest::STATUS_IN_PROGRESS);
        $accountRequestRiri->setBankerResponder($this->getReference("banker"));
        $accountRequestRiri->setFilename('0c17e18f-a8b7-4e33-95d8-cd23947824b6.jpg');
        $accountRequestRiri->setOriginalFileName('id-card-trump.jpg');
        $accountRequestRiri->setUserTransmitter($this->getReference("verified"));

        $manager->persist($accountRequestRiri);

        $accountRequestRiri->setId('0c17e18f-a8b7-4e33-95d8-cd23947824b6');


        for ($i = 0; $i < 5; $i++) {
            $uuid = $faker->uuid;
            $accountRequest = new AccountRequest();
            $accountRequest->setType(AccountRequest::TYPE_NEW_ACCOUNT);
            $accountRequest->setStatus(AccountRequest::STATUS_IN_PROGRESS);
            $accountRequest->setBankerResponder($this->getReference("banker"));
            $accountRequest->setUserTransmitter($this->getReference('user' . random_int(0,4)));
            $accountRequest->setFilename($uuid . '.jpg');
            $accountRequest->setOriginalFileName('id-card-trump.jpg');

            copy(self::FIXTURES_DIRECTORY . $accountRequest->getOriginalFileName(),
                $this->filesFolder . $accountRequest->getFilename());

            $manager->persist($accountRequest);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
