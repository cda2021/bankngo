<?php

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Recipient;
use App\DataFixtures\UserFixtures;
use DateTime;
use Faker\Factory;

class RecipientFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 5; $i++) {
            # creation of a new User object
            $recipient = new Recipient();
            $recipient->setTransmitter($this->getReference("user{$i}"));
            $recipient->setIban($faker->iban());
            $recipient->setName($faker->lastName);
            $recipient->setSurname($faker->firstName);

            $this->setReference("recipient{$i}", $recipient);
            # persits data into db
            $manager->persist($recipient);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
