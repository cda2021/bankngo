<?php

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use DateTime;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $adminOucema = new User();
        $adminOucema->setEmail('oucema@cfa-afti.fr');
        $adminOucema->setBirthDate(new DateTime('-20 years'));
        $adminOucema->setPassword($this->encoder->encodePassword($adminOucema, "oucema"));
        $adminOucema->setGender(User::MALE_GENDER);
        $adminOucema->setName("Oucema");
        $adminOucema->setSurname("Jlaiel");
        $adminOucema->setRole("ROLE_ADMIN");
        $adminOucema->setActive(true);
        $adminOucema->setIsVerified(true);
        $adminOucema->setUniqueLogin("5fd7734e768b1");
        $this->setReference("admin-oucema", $adminOucema);

        $adminRemi = new User();
        $adminRemi->setEmail('remi@cfa-afti.fr');
        $adminRemi->setBirthDate(new DateTime('-20 years'));
        $adminRemi->setPassword($this->encoder->encodePassword($adminRemi, "remi"));
        $adminRemi->setGender(User::MALE_GENDER);
        $adminRemi->setName("Rémi");
        $adminRemi->setSurname("Martinet");
        $adminRemi->setRole("ROLE_ADMIN");
        $adminRemi->setActive(true);
        $adminRemi->setIsVerified(true);
        $adminRemi->setUniqueLogin("5fd7734eaceac");
        $this->setReference("admin-remi", $adminRemi);

        $adminGuillaume = new User();
        $adminGuillaume->setEmail('guillaume@cfa-afti.fr');
        $adminGuillaume->setBirthDate(new DateTime('-20 years'));
        $adminGuillaume->setPassword($this->encoder->encodePassword($adminGuillaume, "guillaume"));
        $adminGuillaume->setGender(User::MALE_GENDER);
        $adminGuillaume->setName("Guillaume");
        $adminGuillaume->setSurname("Kergueris");
        $adminGuillaume->setRole("ROLE_ADMIN");
        $adminGuillaume->setActive(true);
        $adminGuillaume->setIsVerified(true);
        $adminGuillaume->setUniqueLogin("5fd7734ecb5c3");
        $this->setReference("admin-guillaume", $adminGuillaume);

        $verifiedUser = new User();
        $verifiedUser->setEmail("verified@user.com");
        $verifiedUser->setBirthDate(new DateTime('-20 years'));
        $verifiedUser->setPassword($this->encoder->encodePassword($verifiedUser, "riri"));
        $verifiedUser->setGender(User::MALE_GENDER);
        $verifiedUser->setName("Verified");
        $verifiedUser->setSurname("Ri");
        $verifiedUser->setRole("ROLE_CUSTOMER");
        $verifiedUser->setIsVerified(true);
        $verifiedUser->setUniqueLogin("5fd7734f3f048");
        $this->addReference("verified", $verifiedUser);

        $notVerifiedUser = new User();
        $notVerifiedUser->setEmail("not@verified.com");
        $notVerifiedUser->setBirthDate(new DateTime('-20 years'));
        $notVerifiedUser->setPassword($this->encoder->encodePassword($notVerifiedUser, "notverified"));
        $notVerifiedUser->setGender(User::MALE_GENDER);
        $notVerifiedUser->setName("Not");
        $notVerifiedUser->setSurname("Verified");
        $notVerifiedUser->setIsVerified(false);
        $notVerifiedUser->setUniqueLogin("5fd79679c7288");

        $banker = new User();
        $banker->setEmail("banker@bankngo.com");
        $banker->setBirthDate(new DateTime('-20 years'));
        $banker->setPassword($this->encoder->encodePassword($banker, "banker"));
        $banker->setGender(User::MALE_GENDER);
        $banker->setName("The");
        $banker->setSurname("Banker");
        $banker->setIsVerified(true);
        $banker->setRole("ROLE_BANKER");
        $banker->setUniqueLogin("5fd7967abb0e1");
        $this->setReference("banker", $banker);

        // rajout des 4 autres bankers
        // jean
        $jean_banker = new User();
        $jean_banker->setEmail("jean.banker@bankngo.com");
        $jean_banker->setBirthDate(new DateTime('-30 years'));
        $jean_banker->setPassword($this->encoder->encodePassword($jean_banker, "banker"));
        $jean_banker->setGender(User::MALE_GENDER);
        $jean_banker->setName("Dumas");
        $jean_banker->setSurname("Jean");
        $jean_banker->setIsVerified(true);
        $jean_banker->setRole("ROLE_BANKER");
        $jean_banker->setUniqueLogin("3sDkWtUDLF33VqBs");

        $pierre_banker = new User();
        $pierre_banker->setEmail("pierre.banker@bankngo.com");
        $pierre_banker->setBirthDate(new DateTime('-34 years'));
        $pierre_banker->setPassword($this->encoder->encodePassword($pierre_banker, "banker"));
        $pierre_banker->setGender(User::MALE_GENDER);
        $pierre_banker->setName("Voss");
        $pierre_banker->setSurname("Pierre");
        $pierre_banker->setIsVerified(true);
        $pierre_banker->setRole("ROLE_BANKER");
        $pierre_banker->setUniqueLogin("NwbEr9cEYyDx8pZk");

        $alice_banker = new User();
        $alice_banker->setEmail("alice.banker@bankngo.com");
        $alice_banker->setBirthDate(new DateTime('-27 years'));
        $alice_banker->setPassword($this->encoder->encodePassword($alice_banker, "banker"));
        $alice_banker->setGender(User::FEMALE_GENDER);
        $alice_banker->setName("Groupama");
        $alice_banker->setSurname("Alice");
        $alice_banker->setIsVerified(true);
        $alice_banker->setRole("ROLE_BANKER");
        $alice_banker->setUniqueLogin("NJ8m69UWxDaK2zyU");

        $sarah_banker = new User();
        $sarah_banker->setEmail("sarah.banker@bankngo.com");
        $sarah_banker->setBirthDate(new DateTime('-27 years'));
        $sarah_banker->setPassword($this->encoder->encodePassword($sarah_banker, "banker"));
        $sarah_banker->setGender(User::FEMALE_GENDER);
        $sarah_banker->setName("Tahoud");
        $sarah_banker->setSurname("Sarah");
        $sarah_banker->setIsVerified(true);
        $sarah_banker->setRole("ROLE_BANKER");
        $sarah_banker->setUniqueLogin("DyA2UajcvmLfBht3");

        # persits data into db
        $manager->persist($notVerifiedUser);
        $manager->persist($verifiedUser);
        $manager->persist($banker);
        $manager->persist($adminOucema);
        $manager->persist($adminRemi);
        $manager->persist($adminGuillaume);
        $manager->persist($jean_banker);
        $manager->persist($pierre_banker);
        $manager->persist($alice_banker);
        $manager->persist($sarah_banker);

        for ($j = 0; $j < 5; $j++) {
            # creation of a new User object
            $user = new User();
            $user->setEmail($faker->email);
            $user->setBirthDate(new DateTime('-20 years'));
            $user->setPassword($this->encoder->encodePassword($user, "afti2020"));
            $user->setGender(User::MALE_GENDER);
            $user->setName($faker->firstName);
            $user->setSurname($faker->lastName);
            $user->setActive(true);
            $user->setIsVerified(true);
            $user->setRole("ROLE_CUSTOMER");
            $this->setReference("user{$j}", $user);

            $manager->persist($user);

        }

        $manager->flush();
    }
}
