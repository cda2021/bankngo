<?php

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\BankAccount;
use Faker\Factory;

class BankAccountFixtures extends Fixture  implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $bankAccountRiri = new BankAccount();
        $bankAccountRiri->setBalance(random_int(50, 6000));
        $bankAccountRiri->setIban("BE78311444845059");
        $bankAccountRiri->setLabel('Compte courant');
        $bankAccountRiri->setOwner($this->getReference("verified"));

        $this->addReference("verifiedBankAccount", $bankAccountRiri);

        $manager->persist($bankAccountRiri);
        $bankAccountRiri->setId('0dc230a9-02db-4f84-b442-f8d3c4adfcee');


        for ($i = 0; $i < 5; $i++) {
            # creation of a new User object
            $bankAcc = new BankAccount();
            $bankAcc->setBalance(random_int(50, 6000));
            $bankAcc->setIban($faker->iban());
            $bankAcc->setLabel('Compte courant');
            $bankAcc->setOwner($this->getReference("user{$i}"));

            #set the reference
            $this->setReference("accUser{$i}", $bankAcc);
            # persits data into db
            $manager->persist($bankAcc);
        }
        $manager->flush();

    }
    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
