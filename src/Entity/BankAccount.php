<?php

namespace App\Entity;

use App\Entity\Traits\BaseEntityTrait;
use App\Repository\BankAccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Faker\Factory;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=BankAccountRepository::class)
 */
class BankAccount
{
    use BaseEntityTrait;
    use TimestampableEntity;

    public const BANK_IDENTIFICATION_CODE = "BNGOAFRPPXX";
    public const BANK_CODE = "036987";
    public const AGENCY_CODE = "03374";
    public const AGENCY_ADDRESS = "BANKNGO PARIS CDA 03374 (03374)";

    /**
     * @ORM\Column(type="float")
     */
    private $balance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $iban;

    /**
     * @ORM\Column(type="string", length=255)
     * name of the account (for eg : 'Compte Courant', 'livret A', etc)
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="bankAccounts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Transfer::class, mappedBy="bankAccount")
     */
    private $transfers;

    /**
     * @ORM\OneToMany(targetEntity=AccountRequest::class, mappedBy="closedBankAccount", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $closureAccountRequests;

    /**
     * @ORM\Column(type="boolean")
     */
    private $overdraftAuthorized = false;
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $overdraft = 0;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $bic = self::BANK_IDENTIFICATION_CODE;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $bankCode = self::BANK_CODE;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $agencyCode = self::AGENCY_CODE;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $accountNumber;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $key;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    public $agencyAddress = self::AGENCY_ADDRESS;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $active = true;

    public function __construct()
    {
        $faker = Factory::create();
        $this->transfers = new ArrayCollection();
        $this->closureAccountRequests = new ArrayCollection();
        $this->iban = $faker->iban();
        $this->accountNumber = $faker->bankAccountNumber;
        $this->key = random_int(1, 97);
    }

    public function getBalance(): ?float
    {
        return $this->balance;
    }

    public function setBalance(float $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getLabel(): ?string
    {
        return ucfirst($this->label);
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Transfer[]
     */
    public function getTransfers(): Collection
    {
        return $this->transfers;
    }

    public function addTransfer(Transfer $transfer): self
    {
        if (!$this->transfers->contains($transfer)) {
            $this->transfers[] = $transfer;
            $transfer->setBankAccount($this);
        }

        return $this;
    }

    public function removeTransfer(Transfer $transfer): self
    {
        if ($this->transfers->removeElement($transfer)) {
            // set the owning side to null (unless already changed)
            if ($transfer->getBankAccount() === $this) {
                $transfer->setBankAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AccountRequest[]
     */
    public function getClosureAccountRequests(): Collection
    {
        return $this->closureAccountRequests;
    }

    public function addClosureAccountRequest(AccountRequest $accountRequest): self
    {
        if (!$this->closureAccountRequests->contains($accountRequest)) {
            $this->closureAccountRequests[] = $accountRequest;
            $accountRequest->setClosedBankAccount($this);
        }

        return $this;
    }

    public function removeClosureAccountRequest(AccountRequest $accountRequest): self
    {
        if ($this->closureAccountRequests->removeElement($accountRequest)) {
            // set the owning side to null (unless already changed)
            if ($accountRequest->getClosedBankAccount() === $this) {
                $accountRequest->setClosedBankAccount(null);
            }
        }

        return $this;
    }

    public function subtractMoney(float $amount)
    {
        $this->balance = $this->balance - $amount;
    }

    public function addMoney(float $amount)
    {
        $this->balance = $this->balance + $amount;
    }

    public function getOverdraftAuthorized(): ?bool
    {
        return $this->overdraftAuthorized;
    }

    public function setOverdraftAuthorized(bool $overdraftAuthorized): self
    {
        $this->overdraftAuthorized = $overdraftAuthorized;

        return $this;
    }

    public function getOverdraft(): ?float
    {
        return $this->overdraft;
    }

    public function setOverdraft(?float $overdraft): self
    {
        $this->overdraft = $overdraft;

        return $this;
    }

    public function getBic(): string
    {
        return $this->bic;
    }

    public function setBic(string $bic): void
    {
        $this->bic = $bic;
    }

    public function getBankCode(): string
    {
        return $this->bankCode;
    }

    public function setBankCode(string $bankCode): void
    {
        $this->bankCode = $bankCode;
    }

    public function getAgencyCode(): string
    {
        return $this->agencyCode;
    }

    public function setAgencyCode(string $agencyCode): void
    {
        $this->agencyCode = $agencyCode;
    }

    public function getAccountNumber(): string
    {
        return $this->accountNumber;
    }

    public function setAccountNumber(string $accountNumber): void
    {
        $this->accountNumber = $accountNumber;
    }

    public function getKey(): int
    {
        return $this->key;
    }

    public function setKey(int $key): void
    {
        $this->key = $key;
    }

    public function getAgencyAddress(): string
    {
        return $this->agencyAddress;
    }

    public function setAgencyAddress(string $agencyAddress): void
    {
        $this->agencyAddress = $agencyAddress;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }



}
