<?php

namespace App\Entity;

use App\Entity\Traits\BaseEntityTrait;
use App\Repository\RecipientRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=RecipientRequestRepository::class)
 */
class RecipientRequest
{
    use BaseEntityTrait;
    use TimestampableEntity;

    public const STATUS_IN_PROGRESS = 0;
    public const STATUS_REJECTED = 1;
    public const STATUS_ACCEPTED = 2;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $iban;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="recipientRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userTransmitter;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="receivedRecipientRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bankerResponder;

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getName(): ?string
    {
        return ucfirst($this->name);
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return ucfirst($this->surname);
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getUserTransmitter(): ?User
    {
        return $this->userTransmitter;
    }

    public function setUserTransmitter(?User $userTransmitter): self
    {
        $this->userTransmitter = $userTransmitter;

        return $this;
    }

    public function getBankerResponder(): ?User
    {
        return $this->bankerResponder;
    }

    public function setBankerResponder(?User $bankerResponder): self
    {
        $this->bankerResponder = $bankerResponder;

        return $this;
    }
}
