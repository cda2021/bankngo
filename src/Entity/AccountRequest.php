<?php

namespace App\Entity;

use App\Entity\Traits\BaseEntityTrait;
use App\Repository\AccountRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AccountRequestRepository::class)
 * @Vich\Uploadable
 */
class AccountRequest
{
    use BaseEntityTrait;
    use TimestampableEntity;

    public const TYPE_NEW_ACCOUNT = 1;
    public const TYPE_DELETE_ACCOUNT = 2;

    public const STATUS_IN_PROGRESS = 0;
    public const STATUS_REJECTED = 1;
    public const STATUS_ACCEPTED = 2;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @Vich\UploadableField(mapping="accountrequest", fileNameProperty="filename", originalName="originalFileName")
     * @Assert\File(
     *     maxSize="1024k",
     *     mimeTypes={"application/pdf", "application/x-pdf", "image/jpeg", "image/png"},
     *     mimeTypesMessage="Please upload a PDF document or a JPEG/PNG picture."
     * )
     * @var File
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $originalFileName;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="accountRequests")
     */
    private $userTransmitter;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="receivedAccountRequests")
     */
    private $bankerResponder;

    /**
     * @ORM\ManyToOne(targetEntity=BankAccount::class, inversedBy="closureAccountRequests", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $closedBankAccount = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private $overdraftAuthorized = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $score = 0;

    /**
     * @var ?string
     * @ORM\Column(type="text", nullable=true)
     */
    private $ocrText = null;

    /**
     * @var ?string
     * @ORM\Column(type="text", nullable=true)
     */
    private $ocrFindWords = null;

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getOriginalFileName(): ?string
    {
        return $this->originalFileName;
    }

    public function setOriginalFileName(string $originalFileName): self
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUserTransmitter(): ?User
    {
        return $this->userTransmitter;
    }

    public function setUserTransmitter(?User $userTransmitter): self
    {
        $this->userTransmitter = $userTransmitter;

        return $this;
    }

    public function getBankerResponder(): ?User
    {
        return $this->bankerResponder;
    }

    public function setBankerResponder(?User $bankerResponder): self
    {
        $this->bankerResponder = $bankerResponder;

        return $this;
    }

    /**
     * @param File|UploadedFile|null $file
     * @throws \Exception
     */
    public function setFile(?File $file = null): void
    {
        $this->file = $file;

        if (null !== $file) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function getClosedBankAccount(): ?BankAccount
    {
        return $this->closedBankAccount;
    }

    public function setClosedBankAccount(?BankAccount $closedBankAccount): self
    {
        $this->closedBankAccount = $closedBankAccount;

        return $this;
    }

    public function getOverdraftAuthorized(): ?bool
    {
        return $this->overdraftAuthorized;
    }

    public function setOverdraftAuthorized(bool $overdraftAuthorized): self
    {
        $this->overdraftAuthorized = $overdraftAuthorized;

        return $this;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function setScore(int $score): void
    {
        $this->score = $score;
    }

    public function getOcrText(): ?string
    {
        return $this->ocrText;
    }

    public function setOcrText($ocrText): void
    {
        $this->ocrText = $ocrText;
    }

    public function getOcrFindWords(): ?string
    {
        return $this->ocrFindWords;
    }

    public function setOcrFindWords($ocrFindWords): void
    {
        $this->ocrFindWords = $ocrFindWords;
    }
}
