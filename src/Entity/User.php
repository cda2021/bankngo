<?php

namespace App\Entity;

use App\Entity\Traits\BaseEntityTrait;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface, EquatableInterface
{
    use BaseEntityTrait;
    use TimestampableEntity;

    public const MALE_GENDER = 'M';
    public const FEMALE_GENDER = 'F';

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     */
    private $role = '';

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string | null
     */
    private $plainPassword = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="date")
     * @Assert\LessThanOrEqual("-18 years", message="You should be at least 18 years old.")
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $gender;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active = true;

    /**
     * @ORM\OneToMany(targetEntity=BankAccount::class, mappedBy="owner")
     */
    private $bankAccounts;

    /**
     * @ORM\OneToMany(targetEntity=Recipient::class, mappedBy="transmitter")
     */
    private $recipients;

    /**
     * @ORM\OneToMany(targetEntity=AccountRequest::class, mappedBy="userTransmitter")
     */
    private $accountRequests;

    /**
     * @ORM\OneToMany(targetEntity=RecipientRequest::class, mappedBy="userTransmitter")
     */
    private $recipientRequests;

    /**
     * @ORM\OneToMany(targetEntity=RecipientRequest::class, mappedBy="bankerResponder")
     */
    private $receivedRecipientRequests;

    /**
     * @ORM\OneToMany(targetEntity=AccountRequest::class, mappedBy="bankerResponder")
     */
    private $receivedAccountRequests;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $passwordResetToken = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private $twoFactorAuthentification = false;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=120, unique=true)
     */
    private $uniqueLogin;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $googleAuthenticatorSecret;

    /**
     * @var ?string
     * @ORM\Column(type="string", nullable=true)
     */
    private $twoFactorAuthenticationResetToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastVerifyEmailResetAt;

    public function __construct()
    {
        $this->bankAccounts = new ArrayCollection();
        $this->recipients = new ArrayCollection();
        $this->accountRequests = new ArrayCollection();
        $this->recipientRequests = new ArrayCollection();
        $this->receivedRecipientRequests = new ArrayCollection();
        $this->receivedAccountRequests = new ArrayCollection();
        $this->uniqueLogin = uniqid();
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = [$this->role, 'ROLE_USER'];
        // guarantee every user at least has ROLE_USER

        return array_unique($roles);
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return ucfirst($this->name);
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return ucfirst($this->surname);
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|BankAccount[]
     */
    public function getBankAccounts(): Collection
    {
        return $this->bankAccounts;
    }

    public function addBankAccount(BankAccount $bankAccount): self
    {
        if (!$this->bankAccounts->contains($bankAccount)) {
            $this->bankAccounts[] = $bankAccount;
            $bankAccount->setOwner($this);
        }

        return $this;
    }

    public function removeBankAccount(BankAccount $bankAccount): self
    {
        if ($this->bankAccounts->removeElement($bankAccount)) {
            // set the owning side to null (unless already changed)
            if ($bankAccount->getOwner() === $this) {
                $bankAccount->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Recipient[]
     */
    public function getRecipients(): Collection
    {
        return $this->recipients;
    }

    public function addRecipient(Recipient $recipient): self
    {
        if (!$this->recipients->contains($recipient)) {
            $this->recipients[] = $recipient;
            $recipient->setTransmitter($this);
        }

        return $this;
    }

    public function removeRecipient(Recipient $recipient): self
    {
        if ($this->recipients->removeElement($recipient)) {
            // set the owning side to null (unless already changed)
            if ($recipient->getTransmitter() === $this) {
                $recipient->setTransmitter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AccountRequest[]
     */
    public function getAccountRequests(): Collection
    {
        return $this->accountRequests;
    }

    public function addAccountRequest(AccountRequest $accountRequest): self
    {
        if (!$this->accountRequests->contains($accountRequest)) {
            $this->accountRequests[] = $accountRequest;
            $accountRequest->setUserTransmitter($this);
        }

        return $this;
    }

    public function removeAccountRequest(AccountRequest $accountRequest): self
    {
        if ($this->accountRequests->removeElement($accountRequest)) {
            // set the owning side to null (unless already changed)
            if ($accountRequest->getUserTransmitter() === $this) {
                $accountRequest->setUserTransmitter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RecipientRequest[]
     */
    public function getRecipientRequests(): Collection
    {
        return $this->recipientRequests;
    }

    public function addRecipientRequest(RecipientRequest $recipientRequest): self
    {
        if (!$this->recipientRequests->contains($recipientRequest)) {
            $this->recipientRequests[] = $recipientRequest;
            $recipientRequest->setUserTransmitter($this);
        }

        return $this;
    }

    public function removeRecipientRequest(RecipientRequest $recipientRequest): self
    {
        if ($this->recipientRequests->removeElement($recipientRequest)) {
            // set the owning side to null (unless already changed)
            if ($recipientRequest->getUserTransmitter() === $this) {
                $recipientRequest->setUserTransmitter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RecipientRequest[]
     */
    public function getReceivedRecipientRequests(): Collection
    {
        return $this->receivedRecipientRequests;
    }

    public function addReceivedRecipientRequest(RecipientRequest $receivedRecipientRequest): self
    {
        if (!$this->receivedRecipientRequests->contains($receivedRecipientRequest)) {
            $this->receivedRecipientRequests[] = $receivedRecipientRequest;
            $receivedRecipientRequest->setBankerResponder($this);
        }

        return $this;
    }

    public function removeReceivedRecipientRequest(RecipientRequest $receivedRecipientRequest): self
    {
        if ($this->receivedRecipientRequests->removeElement($receivedRecipientRequest)) {
            // set the owning side to null (unless already changed)
            if ($receivedRecipientRequest->getBankerResponder() === $this) {
                $receivedRecipientRequest->setBankerResponder(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AccountRequest[]
     */
    public function getReceivedAccountRequests(): Collection
    {
        return $this->receivedAccountRequests;
    }

    public function addReceivedAccountRequest(AccountRequest $receivedAccountRequest): self
    {
        if (!$this->receivedAccountRequests->contains($receivedAccountRequest)) {
            $this->receivedAccountRequests[] = $receivedAccountRequest;
            $receivedAccountRequest->setBankerResponder($this);
        }

        return $this;
    }

    public function removeReceivedAccountRequest(AccountRequest $receivedAccountRequest): self
    {
        if ($this->receivedAccountRequests->removeElement($receivedAccountRequest)) {
            // set the owning side to null (unless already changed)
            if ($receivedAccountRequest->getBankerResponder() === $this) {
                $receivedAccountRequest->setBankerResponder(null);
            }
        }

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getPasswordResetToken(): ?string
    {
        return $this->passwordResetToken;
    }

    public function setPasswordResetToken(?string $passwordResetToken): self
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    public function getTwoFactorAuthentification(): ?bool
    {
        return $this->twoFactorAuthentification;
    }

    public function setTwoFactorAuthentification(bool $twoFactorAuthentification): self
    {
        $this->twoFactorAuthentification = $twoFactorAuthentification;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getUniqueLogin(): string
    {
        return $this->uniqueLogin;
    }

    public function setUniqueLogin(string $uniqueLogin): self
    {
        $this->uniqueLogin = $uniqueLogin;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function getGoogleAuthenticatorSecret() :?string
    {
        return $this->googleAuthenticatorSecret;
    }

    public function setGoogleAuthenticatorSecret(?string $googleAuthenticatorSecret): void
    {
        $this->googleAuthenticatorSecret = $googleAuthenticatorSecret;
    }

    public function isEqualTo(UserInterface $user)
    {
        return $this->email === $user->getEmail();
    }

    public function getTwoFactorAuthenticationResetToken(): ?string
    {
        return $this->twoFactorAuthenticationResetToken;
    }

    public function setTwoFactorAuthenticationResetToken(?string $twoFactorAuthenticationResetToken): void
    {
        $this->twoFactorAuthenticationResetToken = $twoFactorAuthenticationResetToken;
    }

    public function isBanker()
    {
        return in_array("ROLE_BANKER", $this->getRoles());
    }

    public function isAdmin()
    {
        return in_array("ROLE_ADMIN", $this->getRoles());
    }

    public function isCustomer()
    {
        return in_array("ROLE_CUSTOMER", $this->getRoles());
    }

    public function getLastVerifyEmailResetAt()
    {
        return $this->lastVerifyEmailResetAt;
    }

    public function setLastVerifyEmailResetAt($lastVerifyEmailResetAt): void
    {
        $this->lastVerifyEmailResetAt = $lastVerifyEmailResetAt;
    }
}
