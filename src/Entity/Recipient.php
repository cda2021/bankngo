<?php

namespace App\Entity;

use App\Entity\Traits\BaseEntityTrait;
use App\Repository\RecipientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=RecipientRepository::class)
 */
class Recipient
{
    use BaseEntityTrait;
    use TimestampableEntity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $iban;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="recipients")
     * @ORM\JoinColumn(nullable=false)
     */
    private $transmitter;

    /**
     * @ORM\OneToMany(targetEntity=Transfer::class, mappedBy="recipient")
     */
    private $receivedTransfers;

    public function __construct()
    {
        $this->receivedTransfers = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getTransmitter(): ?User
    {
        return $this->transmitter;
    }

    public function setTransmitter(?User $transmitter): self
    {
        $this->transmitter = $transmitter;

        return $this;
    }

    /**
     * @return Collection|Transfer[]
     */
    public function getReceivedTransfers(): Collection
    {
        return $this->receivedTransfers;
    }

    public function addReceivedTransfer(Transfer $receivedTransfer): self
    {
        if (!$this->receivedTransfers->contains($receivedTransfer)) {
            $this->receivedTransfers[] = $receivedTransfer;
            $receivedTransfer->setRecipient($this);
        }

        return $this;
    }

    public function removeReceivedTransfer(Transfer $receivedTransfer): self
    {
        if ($this->receivedTransfers->removeElement($receivedTransfer)) {
            // set the owning side to null (unless already changed)
            if ($receivedTransfer->getRecipient() === $this) {
                $receivedTransfer->setRecipient(null);
            }
        }

        return $this;
    }
}
